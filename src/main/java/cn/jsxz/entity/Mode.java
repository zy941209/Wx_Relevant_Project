package cn.jsxz.entity;


public class Mode {

    private String orderId;

    private Integer money;

    private Integer teamId;

    private Integer flag;


    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Integer getMoney() {
        return money;
    }

    public void setMoney(Integer money) {
        this.money = money;
    }

    public Integer getTeamId() {
        return teamId;
    }

    public void setTeamId(Integer teamId) {
        this.teamId = teamId;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    @Override
    public String toString() {
        return "Mode{" +
                "orderId='" + orderId + '\'' +
                ", money=" + money +
                ", teamId=" + teamId +
                ", flag=" + flag +
                '}';
    }
}
