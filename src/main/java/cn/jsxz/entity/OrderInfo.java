package cn.jsxz.entity;

public class OrderInfo {

    private String appid; // 微信分配的小程序ID
    private String openid; // trade_type=JSAPI，此参数必传，用户在商户appid下的唯一标识。openid如何获取，可参考【获取openid】。
    private String sign; // 通过签名算法计算得出的签名值，详见签名生成算法
    private String body; // 商品简单描述，该字段请按照规范传递，具体请见参数规定
    private String mch_id;// 微信支付分配的商户号
    private int total_fee; // 订单总金额，单位为分，详见支付金额
    private String sign_type; // 签名类型，默认为MD5，支持HMAC-SHA256和MD5。
    private String trade_type; // 小程序取值如下：JSAPI，详细说明见参数规定
    private String notify_url; // 异步接收微信支付结果通知的回调地址，通知url必须为外网可访问的url，不能携带参数。
    private String nonce_str; // 随机字符串，长度要求在32位以内。推荐随机数生成算法
    private String out_trade_no; // 商户系统内部订单号，要求32个字符内，只能是数字、大小写字母_-|*且在同一个商户号下唯一。详见商户订单号
    private String spbill_create_ip; // APP和网页支付提交用户端ip，Native支付填调用微信支付API的机器IP。

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getOut_trade_no() {
        return out_trade_no;
    }

    public void setOut_trade_no(String out_trade_no) {
        this.out_trade_no = out_trade_no;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getMch_id() {
        return mch_id;
    }

    public void setMch_id(String mch_id) {
        this.mch_id = mch_id;
    }

    public String getTrade_type() {
        return trade_type;
    }

    public void setTrade_type(String trade_type) {
        this.trade_type = trade_type;
    }

    public String getNotify_url() {
        return notify_url;
    }

    public void setNotify_url(String notify_url) {
        this.notify_url = notify_url;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public int getTotal_fee() {
        return total_fee;
    }

    public void setTotal_fee(int total_fee) {
        this.total_fee = total_fee;
    }

    public String getSign_type() {
        return sign_type;
    }

    public void setSign_type(String sign_type) {
        this.sign_type = sign_type;
    }

    public String getSpbill_create_ip() {
        return spbill_create_ip;
    }

    public void setSpbill_create_ip(String spbill_create_ip) {
        this.spbill_create_ip = spbill_create_ip;
    }

    public String getNonce_str() {
        return nonce_str;
    }

    public void setNonce_str(String nonce_str) {
        this.nonce_str = nonce_str;
    }
}
