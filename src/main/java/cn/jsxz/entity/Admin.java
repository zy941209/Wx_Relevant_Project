package cn.jsxz.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

/**
 * @ClassName: Admin
 * @Description: TODO
 */
@TableName("admin")
public class Admin extends Model<Admin> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "admin_id", type = IdType.INPUT)
    private String adminId;

    @TableField("login_name")
    private String loginName;// 登录账号

    @TableField("login_pwd")
    private String loginPwd;// 登录密码

    @TableField("name")
    private String name;//姓名

    @TableField("phone")
    private String phone;// 手机号

    @TableField("photo")
    private String photo;// 用户头像

    @TableField("admin_leve")
    private Integer adminLeve;//管理员身份等级1超级管理员2子管理员

    @TableField("del_flag")
    private Integer delFlag;//删除标识（0未删除，1已删除）

    @TableField("forbidden_flag")
    private Integer forbiddenFlag;//禁用标记，0启用，1禁用

    @TableField("create_by")
    private String createBy;// 创建人

    @TableField("create_time")
    private String createTime;// 创建时间

    @TableField("update_time")
    private String updateTime;// 修改时间

    @TableField("login_time")
    private String loginTime;// 上次登陆时间

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getLoginPwd() {
        return loginPwd;
    }

    public void setLoginPwd(String loginPwd) {
        this.loginPwd = loginPwd;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Integer getAdminLeve() {
        return adminLeve;
    }

    public void setAdminLeve(Integer adminLeve) {
        this.adminLeve = adminLeve;
    }

    public Integer getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Integer delFlag) {
        this.delFlag = delFlag;
    }

    public Integer getForbiddenFlag() {
        return forbiddenFlag;
    }

    public void setForbiddenFlag(Integer forbiddenFlag) {
        this.forbiddenFlag = forbiddenFlag;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(String loginTime) {
        this.loginTime = loginTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.adminId;
    }

    @Override
    public String toString() {
        return "Admin{" +
                "adminId='" + adminId + '\'' +
                ", loginName='" + loginName + '\'' +
                ", loginPwd='" + loginPwd + '\'' +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", photo='" + photo + '\'' +
                ", adminLeve=" + adminLeve +
                ", delFlag=" + delFlag +
                ", forbiddenFlag=" + forbiddenFlag +
                ", createBy='" + createBy + '\'' +
                ", createTime='" + createTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                ", loginTime='" + loginTime + '\'' +
                '}';
    }
}
