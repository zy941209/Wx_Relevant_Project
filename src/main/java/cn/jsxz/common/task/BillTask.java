package cn.jsxz.common.task;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 定时器
 */
@Component
public class BillTask {

    @Scheduled(cron = "50 50 23 * * *")
    public void billTask() {
    }
}
