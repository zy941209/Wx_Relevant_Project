package cn.jsxz.common.constants;

/**
 * 常量接口
 *
 * @author zy
 * @ClassName: Constants
 * @Description: TODO
 * @date 2018年7月24日
 */
public interface Constants {

    ////////////// 系统标识符 开始//////////////////
    /**
     * 错误 描述
     */
    String MSG_ERROR = "error";
    /**
     * 成功 描述
     */
    String MSG_SUCCESS = "success";

    ////////////// 系统状态码 开始//////////////////
    /**
     * 请求失败
     */
    int ERROR = 400;
    /**
     * 请求成功
     */
    int SUCCESS = 200;

    /**
     * 服务器内部错误
     */
    int SERVER_ERROR = 500;

    /**
     * 提示信息代码
     *
     * @author zy
     * @ClassName: HTTP_STATE_CODE
     * @Description: TODO
     * @date 2018年7月24日
     */
    public interface HTTP_STATE_CODE {
        /**
         * 访问成功
         */
        int SUCCESS_200 = 200;
        /**
         * 错误请求
         */
        int ERROR_400 = 400;
        /**
         * 服务器内部错误
         */
        int ERROR_500 = 500;
        /**
         * 无法找到指定位置的资源
         */
        int ERROR_404 = 404;

        int ERROR_300 = 300;
    }

    /**
     * 描述提醒
     *
     * @author zy
     * @ClassName: HTTP_STATE_MASSAGE
     * @Description: TODO
     * @date 2018年7月24日
     */
    public interface HTTP_STATE_MASSAGE {
        /**
         * 成功
         */
        String SUCCESS_200_MSG = "成功";
        String ERROR_300_MSG = "请求失败";
        String SUCCESS_LOGIN_MSG = "登陆成功";
        String SUCCESS_LOGOUT_MSG = "退出成功";
        String ERROR_LOGIN_MSG = "登陆失败";
        String ERROR_400_MSG = "错误请求";
        String ERROR_500_MSG = "服务器内部错误";
        String ERROR_ADD_MSG = "新增失败";
        String ERROR_EDIT_MSG = "修改失败";
        String ERROR_DELETE_MSG = "删除失败";
        String ERROR_SELECT_MSG = "查询失败";
    }
}
