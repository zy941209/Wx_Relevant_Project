package cn.jsxz.common.utils.Queue;

import cn.jsxz.entity.Mode;

import java.util.concurrent.LinkedBlockingQueue;

/**
 * 创建队列
 */
public class TaskQueue {
    private static LinkedBlockingQueue<Mode> queues = null;

    //创建一个无大小限制的队列
    public static LinkedBlockingQueue getTaskQueue() {
        if (queues == null) {
            queues = new LinkedBlockingQueue<>();
        }
        return queues;
    }

    //入队操作
    public static void add(Mode mode) {
        if (queues == null)
            queues = getTaskQueue();
        try {
            queues.put(mode);//入队操作
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
