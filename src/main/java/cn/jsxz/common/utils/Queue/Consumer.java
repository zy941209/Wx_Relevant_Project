package cn.jsxz.common.utils.Queue;


/**
 * @auther: Zy
 * 功能描述: 出队，处理逻辑操作
 * @date: 2019
 */
public class Consumer implements Runnable {

    private static Consumer consumer;

    public synchronized void run() {
        System.out.println(Thread.currentThread().isInterrupted());
        while (Thread.currentThread().isInterrupted() == false) {
            try {
                TaskQueue taskQueue = new TaskQueue();
                taskQueue.getTaskQueue().take();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static Consumer getInstance() {
        if (consumer == null) {
            consumer = new Consumer();
        }
        return consumer;
    }
}
