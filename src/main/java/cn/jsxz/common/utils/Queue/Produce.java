package cn.jsxz.common.utils.Queue;

import cn.jsxz.entity.Mode;


/**
 * 入队线程
 */
public class Produce implements Runnable {
    private Mode mode;

    //塞参数
    public void setMode(Mode mode) {
        this.mode = mode;
    }

    public synchronized void run() {
        TaskQueue.add(mode);//添加到队列里
    }
}
