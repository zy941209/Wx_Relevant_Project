package cn.jsxz.common.utils.Queue;

import cn.jsxz.entity.Mode;

import java.util.concurrent.ExecutorService;

/**
 * @auther: Zy
 * @method 功能描述:队列测试
 * @date: 2019-01-09 17:12:19
 */
public class Test {
    public static void main(String[] args) throws InterruptedException {
        ExecutorService threadPool = ThreadPool.getThreadPool();//获取线程池

        Produce produce = new Produce();
        Mode mode = new Mode();
        mode.setOrderId("1");
        mode.setMoney(100);
        mode.setTeamId(1);
        produce.setMode(mode);
        threadPool.execute(produce);
        Thread.sleep(2000);
        Produce produce1 = new Produce();
        Mode mode1 = new Mode();
        mode1.setOrderId("2");
        mode1.setMoney(200);
        mode1.setTeamId(2);
        produce1.setMode(mode1);
        threadPool.execute(produce1);
        Thread.sleep(2000);
        Produce produce2 = new Produce();
        Mode mode2 = new Mode();
        mode2.setOrderId("3");
        mode2.setMoney(300);
        mode2.setTeamId(3);
        produce2.setMode(mode2);
        threadPool.execute(produce2);

        Thread.sleep(2000);
        Consumer consumer = Consumer.getInstance();
        threadPool.execute(consumer);
    }
}
