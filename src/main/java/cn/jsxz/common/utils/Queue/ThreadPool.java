package cn.jsxz.common.utils.Queue;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 创建线程池
 */
public class ThreadPool {
    private static ExecutorService threadPool = null;

    public static ExecutorService getThreadPool() {
        if (threadPool == null) {
            threadPool = Executors.newCachedThreadPool();//创建一个无大小限制的线程池
        }
        return threadPool;
    }
}
