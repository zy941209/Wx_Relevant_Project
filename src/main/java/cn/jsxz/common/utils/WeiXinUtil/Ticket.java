package cn.jsxz.common.utils.WeiXinUtil;

public class Ticket {

    // 临时二维码
    private final static String QR_SCENE = "QR_SCENE";
    // 永久二维码
    private final static String QR_LIMIT_SCENE = "QR_LIMIT_SCENE";
    // 永久二维码(字符串)
    private final static String QR_LIMIT_STR_SCENE = "QR_LIMIT_STR_SCENE";
    // 创建二维码
    private String create_ticket_path = "https://api.WeiXinUtil.qq.com/cgi-bin/WeiXinUtil/create";
    // 通过ticket换取二维码
    private String showqrcode_path = "https://mp.WeiXinUtil.qq.com/cgi-bin/showqrcode";


}
