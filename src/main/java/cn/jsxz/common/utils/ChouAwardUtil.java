package cn.jsxz.common.utils;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 抽奖工具类<br/>
 * 整体思想：
 * 奖品集合 + 概率比例集合
 * 将奖品按集合中顺序概率计算成所占比例区间，放入比例集合。并产生一个随机数加入其中，排序。</br>
 * 排序后，随机数落在哪个区间，就表示那个区间的奖品被抽中。</br>
 * 返回的随机数在集合中的索引，该索引就是奖品集合中的索引。</br>
 * 比例区间的计算通过概率相加获得。
 */
public class ChouAwardUtil {

    //概率添加到集合中
//    public static int drawGift(List<Award> giftList) {
//        if (null != giftList && giftList.size() > 0) {
//            List<Double> orgProbList = new ArrayList<Double>(giftList.size());
//            for (TAward tAward : giftList) {
//                //按顺序将概率添加到集合中
//                orgProbList.add(tAward.getProportion());
//            }
//            return draw(orgProbList);
//        }
//        return -1;
//    }

    //抽奖
    public static int draw(List<Double> giftProbList) {
        List<Double> sortRateList = new ArrayList<Double>();
        // 计算概率总和
        Double sumRate = 0D;
        for (Double prob : giftProbList) {
            sumRate += prob;
        }
        if (sumRate != 0) {
            double rate = 0D;   //概率所占比例
            for (Double prob : giftProbList) {
                rate += prob;
                // 构建一个比例区段组成的集合(避免概率和不为1)
                sortRateList.add(rate / sumRate);
            }
            // 随机生成一个随机数，并排序
            double random = Math.random();
            sortRateList.add(random);
            Collections.sort(sortRateList);

            // 返回该随机数在比例集合中的索引
            return sortRateList.indexOf(random);
        }
        return -1;
    }

    //测试
//    public static void main(String[] args) {
//        TAward tAward1 = new TAward();
//        tAward1.setAwardId(1);
//        tAward1.setAwardName("1等奖");
//        tAward1.setAwardContent("1等奖奖品");
//        tAward1.setProportion(1.0);
//        tAward1.setAwardCount(1);
//
//        TAward tAward2 = new TAward();
//        tAward2.setAwardId(2);
//        tAward2.setAwardName("二等奖");
//        tAward2.setAwardContent("二等奖奖品");
//        tAward2.setProportion(5.0);
//        tAward2.setAwardCount(5);
//
//        TAward tAward3 = new TAward();
//        tAward3.setAwardId(3);
//        tAward3.setAwardName("三等奖");
//        tAward3.setAwardContent("三等奖奖品");
//        tAward3.setProportion(15.0);
//        tAward3.setAwardCount(15);
//
//        TAward tAward4 = new TAward();
//        tAward4.setAwardId(4);
//        tAward4.setAwardName("四等奖");
//        tAward4.setAwardContent("四等奖奖品");
//        tAward4.setProportion(25.0);
//        tAward4.setAwardCount(25);
//
//        TAward tAward5 = new TAward();
//        tAward5.setAwardId(5);
//        tAward5.setAwardName("五等奖");
//        tAward5.setAwardContent("五等奖奖品");
//        tAward5.setProportion(34.0);
//        tAward5.setAwardCount(34);
//
//        TAward tAward6 = new TAward();
//        tAward6.setAwardId(6);
//        tAward6.setAwardName("六等奖");
//        tAward6.setAwardContent("六等奖奖品");
//        tAward6.setProportion(50.5);
//        tAward6.setAwardCount(50);
//
//        List<TAward> list = new ArrayList<TAward>();
//        list.add(tAward1);
//        list.add(tAward2);
//        list.add(tAward3);
//        list.add(tAward4);
//        list.add(tAward5);
//        list.add(tAward6);

//        for(int i=0;i<1;i++){
//            int index = drawGift(list);
//            System.out.println(index);
//            System.out.println(list.get(index).getAwardId());
//            System.out.println(list.get(index).getAwardName());
//        }
//    }
}