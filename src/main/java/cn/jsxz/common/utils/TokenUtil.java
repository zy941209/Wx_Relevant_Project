package cn.jsxz.common.utils;

import cn.jsxz.common.constants.Constants;
import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson.JSONObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

/**
 * @auther: Zy
 * @method TokenUtil
 * 功能描述: 防重复提交令牌
 * @date: 2019
 */
public class TokenUtil {
    /**
     * @auther: Zy
     * @method setToken
     * 功能描述: 设置令牌
     * @params: [request]
     * @return: * @return : void
     * @date: 2019-01-09 16:31:13
     */
    public static void setToken(HttpServletRequest request) {
        request.getSession().setAttribute("sesToken", RandomUtil.simpleUUID());
    }

    /**
     * @auther: Zy
     * @method getToken
     * 功能描述:
     * @params: [request]
     * @return: * @return : java.lang.String
     * @date: 2019
     */
    public static String getToken(HttpServletRequest request) {
        String sessionToken = (String) request.getSession().getAttribute("sesToken");
        if (null == sessionToken || "".equals(sessionToken)) {
            sessionToken = RandomUtil.simpleUUID();
            request.getSession().setAttribute("sesToken", sessionToken);
        }
        return sessionToken;
    }

    /**
     * @auther: Zy
     * @method validToken
     * 功能描述: 验证是否为重复提交
     * @params: [request]
     * @return: * @return : boolean true重复提交,false非重复提交
     * @date: 2019
     */
    public static boolean validToken(HttpServletRequest request) {
        String sessionToken = (String) request.getSession().getAttribute("sesToken");
        String requestToken = request.getParameter("sesToken");
        if (null == sessionToken || "null".equals(sessionToken)) {
            sessionToken = "";
        }
        if (null == requestToken || "null".equals(requestToken)) {
            requestToken = "";
        }
        if (sessionToken.equals(requestToken)) {
            //返回前一定要重置session中的SesToken
            request.getSession().setAttribute("sesToken", RandomUtil.simpleUUID());
            //非重复提交
            return false;
        } else {
            //返回前一定要重置session中的SesToken
            request.getSession().setAttribute("sesToken", RandomUtil.simpleUUID());
            //重复提交
            return true;
        }
    }

    /**
     * @auther: Zy
     * @method intoInfo
     * 功能描述: 测试生成token
     * @params: [request]
     * @return: * @return : com.alibaba.fastjson.JSONObject
     * @date: 2019
     */
    @RequestMapping(value = "intoInfo", method = RequestMethod.POST)
    public JSONObject intoInfo(HttpServletRequest request) {
        String token = TokenUtil.getToken(request);
        return ResultUtil.resultJSON(Constants.HTTP_STATE_CODE.SUCCESS_200, Constants.HTTP_STATE_MASSAGE.SUCCESS_200_MSG, token);
    }

    /**
     * @auther: Zy
     * @method testToken
     * 功能描述: 防重复提交测试
     * @params: [request]
     * @return: * @return : com.alibaba.fastjson.JSONObject
     * @date: 2019
     */
    @RequestMapping(value = "testToken", method = RequestMethod.POST)
    public JSONObject testToken(HttpServletRequest request) {
        //防重复验证
        if (TokenUtil.validToken(request)) {
            return ResultUtil.resultJSON(Constants.HTTP_STATE_CODE.ERROR_300, "请不要重复提交！", null);
        }
        return ResultUtil.resultJSON(Constants.HTTP_STATE_CODE.SUCCESS_200, Constants.HTTP_STATE_MASSAGE.SUCCESS_200_MSG, null);
    }
}