package cn.jsxz.common.utils.NumberUtil;

import java.math.BigDecimal;
import java.util.regex.Pattern;

/**
 * 自定义数字处理工具类
 *
 * @ClassName: NumberUtil
 * @Description: TODO
 * @author zy
 * @date 2018年7月24日
 *
 */
public class NumberUtil {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	}


	public static int isNum(String str){
		Pattern pattern = Pattern.compile("^[-\\+]?[\\d]*$");
		if(pattern.matcher(str).matches()){
			return Integer.parseInt(str);
		}
		return 0;
	}


	/**
	 * 获取随机数
	 *
	 * @param length
	 * @return
	 */
	public static String getRandom(int length) {
		String con = "";
		int le=1;
		for (int i = 0; i < length; i++) {
			le=le*10;

		}
		con = (int) (Math.random() * le) + "";
		con = con.length() < length ? con + "0" : con;
		return con;
	}
	public static int getInt(Object o) {
		if (o instanceof BigDecimal) {
			return ((BigDecimal) o).intValue();
		} else if (o instanceof Integer) {
			return ((Integer) o).intValue();
		} else if (o instanceof Float) {
			return ((Float) o).intValue();
		} else if (o instanceof Double) {
			return ((Double) o).intValue();
		}

		throw new RuntimeException("can't parse object to int");
	}

	/**
	 * double类型数字验证为null时返回0D
	 *
	 * @param doub
	 * @return
	 */
	public static Double isNull(Double doub) {
		if (null == doub) {
			doub = 0D;
		}
		return doub;
	}

	/**
	 * int类型数字验证为null时返回0
	 *
	 * @param inte
	 * @return
	 */
	public static Integer isNull(Integer inte) {
		if (null == inte) {
			inte = 0;
		}
		return inte;
	}
}
