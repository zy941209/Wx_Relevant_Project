package cn.jsxz.common.utils.NumberUtil;

import java.text.DecimalFormat;
import java.util.Random;

/**
 * 随机数工具类
 */
public class RandomDiyUtil {
    public static void main(String[] args) {
        System.out.println(toSerialCode(6));
    }

    /**
     * 指定区间随机数
     *
     * @param min 最小值
     * @param max 最大值
     * @return double 随机数
     */
    public static double getRanomMinToMax(int min, int max) {
        DecimalFormat df = new DecimalFormat("######0.00");
        if (max == 0) {
            return Double.parseDouble(df.format(0D));
        }
        Random random = new Random();
        double s = random.nextDouble();//随机一个小数
        int num = random.nextInt(max) % (max - min) + min;//随机一个整数
        double double1 = Double.parseDouble(df.format(num + s));//取和后转换保留两位小数
        return double1;
    }

    /**
     * 保留两位小数
     *
     * @param d
     * @return
     */
    @SuppressWarnings("unused")
    public static double doubleTo2(double d) {
        DecimalFormat df = new DecimalFormat("######0.00");
        Random random = new Random();
        double s = random.nextDouble();//随机一个小数
//    	int num=random.nextInt(max)%(max-min) + min;//随机一个整数
        double double1 = Double.parseDouble(df.format(d));//取和后转换保留两位小数
        return double1;
    }


    /**
     * 自定义进制(0,1没有加入,容易与o,l混淆)
     */
    private static final char[] r = new char[]{'q', 'w', 'e', '8', 'a', 's', '2', 'd', 'z', 'x', '9', 'c', '7', 'p', '5', 'i', 'k', '3', 'm', 'j', 'u', 'f', 'r', '4', 'v', 'y', 'l', 't', 'n', '6', 'b', 'g', 'h'};

    /**
     * (不能与自定义进制有重复)
     */
    private static final char b = 'o';

    /**
     * 进制长度
     */
    private static final int binLen = r.length;

    /**
     * 序列最小长度
     */
    private static final int s = 6;

    /**
     * 根据ID生成六位随机码
     *
     * @param id ID
     * @return 随机码
     */
    public static String toSerialCode(long id) {
        char[] buf = new char[32];
        int charPos = 32;

        while ((id / binLen) > 0) {
            int ind = (int) (id % binLen);
            // System.out.println(num + "-->" + ind);
            buf[--charPos] = r[ind];
            id /= binLen;
        }
        buf[--charPos] = r[(int) (id % binLen)];
        // System.out.println(num + "-->" + num % binLen);
        String str = new String(buf, charPos, (32 - charPos));
        // 不够长度的自动随机补全
        if (str.length() < s) {
            StringBuilder sb = new StringBuilder();
            sb.append(b);
            Random rnd = new Random();
            for (int i = 1; i < s - str.length(); i++) {
                sb.append(r[rnd.nextInt(binLen)]);
            }
            str += sb.toString();
        }
        return str;
    }

    public static long codeToId(String code) {
        char chs[] = code.toCharArray();
        long res = 0L;
        for (int i = 0; i < chs.length; i++) {
            int ind = 0;
            for (int j = 0; j < binLen; j++) {
                if (chs[i] == r[j]) {
                    ind = j;
                    break;
                }
            }
            if (chs[i] == b) {
                break;
            }
            if (i > 0) {
                res = res * binLen + ind;
            } else {
                res = ind;
            }
            // System.out.println(ind + "-->" + res);
        }
        return res;
    }
}
