package cn.jsxz.common.utils;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.setting.dialect.Props;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.qiniu.common.QiniuException;
import com.qiniu.common.Zone;
import com.qiniu.http.Response;
import com.qiniu.storage.BucketManager;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.UploadManager;
import com.qiniu.util.Auth;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 七牛文件上传工具类
 *
 * @author zy
 * @ClassName: QiNiuUtil
 * @Description: TODO
 * @date 2018年7月24日
 */
public class QiNiuUtil {

    /**
     * 简单上传，使用默认策略，只需要设置上传的空间名就可以了
     *
     * @param @param  bucketname
     * @param @return 参数
     * @return String    返回类型
     * @throws
     * @Title: getUpToken
     * @Description: TODO
     */
    public static String getUpToken(String bucketname) {
        Props props = new Props("workConfig.properties", "utf-8");
        String acceseKey = props.getStr("acceseKey");
        String sercretKey = props.getStr("sercretKey");
        // 密钥配置
        Auth auth = Auth.create(acceseKey, sercretKey);
        return auth.uploadToken(bucketname);
    }

    /**
     * String类型src
     *
     * @param @param  FilePath
     * @param @return
     * @param @throws QiniuException    参数
     * @return String    返回类型
     * @throws
     * @Title: upload
     * @Description: TODO
     */
    @SuppressWarnings("unused")
    public static String upload(String FilePath) throws QiniuException {
        Props props = new Props("workConfig.properties", "utf-8");
        String acceseKey = props.getStr("acceseKey");
        String sercretKey = props.getStr("sercretKey");
        String bucket = props.getStr("bucket");
        String domain = props.getStr("domain");
        String uuid = RandomUtil.simpleUUID();
        Configuration cfg = new Configuration(Zone.zone0());
        UploadManager uploadManager = new UploadManager(cfg);
        Auth auth = Auth.create(acceseKey, sercretKey);
        String upToken = auth.uploadToken(bucket);
        Response res = uploadManager.put(FilePath, uuid, upToken);
        String imagePath = domain + "/" + uuid;
        cn.hutool.json.JSONObject jsonObject = new cn.hutool.json.JSONObject(res.bodyString());
        StringBuffer buffer = new StringBuffer();
        buffer.append(domain).append(jsonObject.get("key").toString());
        return buffer.toString();
    }


    /**
     * 上传文件到七牛
     *
     * @param @param  multipartFile
     * @param @return 参数
     * @return String    返回类型
     * @throws
     * @Title: upload
     * @Description: TODO
     */
    public static String upload(MultipartFile multipartFile) {
        // 构造一个带指定Zone对象的配置类
        // 机房 Zone对象 华东 Zone.zone0()华北Zone.zone1()华南Zone.zone2()北美Zone.zoneNa0()
        Configuration cfg = new Configuration(Zone.zone0());
        // 创建上传对象
        UploadManager uploadManager = new UploadManager(cfg);
        try {
            Response res = uploadManager.put(multipartFile.getInputStream(), null, getToekn(), null, null);
            // 打印返回的信息
            JSONObject jsonObject = JSONObject.parseObject(res.bodyString());
            return jsonObject.get("key") + "";
        } catch (QiniuException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取七牛Toekn
     *
     * @param @return 参数
     * @return String    返回类型
     * @throws
     * @Title: getToekn
     * @Description: TODO
     */
    public static String getToekn() {
        ReadPropertiesUtil rpu = new ReadPropertiesUtil("workConfig");
        // 七牛的accesskey
        String accessKey = rpu.getProValue("accessKey");
        // 七牛的secrectkey
        String secretKey = rpu.getProValue("secretKey");
        // 空间名称
        String bucket = rpu.getProValue("bucket");
        // 密钥配置
        Auth auth = Auth.create(accessKey, secretKey);
        // 获取token
        String upToken = auth.uploadToken(bucket);
        // 覆盖上传
        // String key = "file key";
        // String upToken = auth.uploadToken(bucket, key);
        // 覆盖上传除了需要简单上传所需要的信息之外，还需要想进行覆盖的文件名称，这个文件名称同时可是客户端上传代码中指定的文件名，两者必须一致。
        // System.out.println(upToken);
        return upToken;
    }

    /**
     * 获取七牛网络地址
     *
     * @param @return 参数
     * @return String    返回类型
     * @throws
     * @Title: getQiNiuURL
     * @Description: TODO
     */
    public static String getQiNiuURL() {
        ReadPropertiesUtil rpu = new ReadPropertiesUtil("workConfig");
        return rpu.getProValue("domain");
    }


    /**
     * 修改上传文件到七牛
     *
     * @param @param  multipartFile
     * @param @param  oldKey 原文件名
     * @param @return 参数
     * @return String    返回类型
     * @throws
     * @Title: update
     * @Description: TODO
     */
    @SuppressWarnings("unused")
    public static String update(MultipartFile multipartFile, String oldKey) {
        // 创建上传对象
        String uploadStatus = upload(multipartFile);// 先上传新文件
        // 如果上传成功，将原有的删除
        if (uploadStatus != null) {
            boolean deleteStatus = delete(oldKey);// 再删除原来文件
        }
        // 打印返回的信息
        JSONObject jsonObject = JSONObject.parseObject(uploadStatus);
        return jsonObject.get("key") + "";
    }

    /**
     * 删除上传文件到七牛
     *
     * @param @param  key 存在七牛上 的对外文件名
     * @param @return 参数
     * @return boolean    返回类型
     * @throws
     * @Title: delete
     * @Description: TODO
     */
    public static boolean delete(String key) {
        // 构造一个带指定Zone对象的配置类
        // 机房 Zone对象 华东 Zone.zone0()华北Zone.zone1()华南Zone.zone2()北美Zone.zoneNa0()
        Configuration cfg = new Configuration(Zone.zone0());
        // 读取配置文件
//		ReadPropertiesUtil rpu = new ReadPropertiesUtil("qiniu");

        Props props = new Props("workConfig.properties", "utf-8");
        // 七牛的accesskey
        String accessKey = props.getStr("acceseKey");
        // 七牛的secrectkey
        String secretKey = props.getStr("sercretKey");
        // 空间名称
        String bucket = props.getStr("bucket");
        // String qiniudomain = props.getStr("domain");
        Auth auth = Auth.create(accessKey, secretKey);
        // String key = "your file key";
        // 定义空间管理
        BucketManager bucketManager = new BucketManager(auth, cfg);
        try {
            // 调用delete方法删除
            Response res = bucketManager.delete(bucket, key);
            return res.isOK();
        } catch (QiniuException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 判断是图片还是视屏 @Title: change @Description: TODO @param @param
     * array @param @return 参数 @return List<Map<String,Object>> 返回类型 @throws
     */
    @SuppressWarnings("unused")
    private static List<Map<String, Object>> isImgOrVideo(JSONArray array) {
        List<Map<String, Object>> listMap = new ArrayList<Map<String, Object>>();
        for (int i = 0; i < array.size(); i++) {
            String src = array.get(i).toString();
            Map<String, Object> map = new HashMap<String, Object>();
            String suffix = src.substring(src.lastIndexOf(".") + 1);
            String[] image = {"jpg", "png", "jpeg", "gif", "bmp"};// 图片格式
            List<String> imgList = Arrays.asList(image);
            String[] video = {"mpg", "mpeg", "avi", "rmvb", "mov", "wmv", "asf", "dat", "asx", "wvx", "mpe", "mpa",
                    "gdf", "mp4", "3gp", "mkv", "flv", "f4v", "m4v", "ts", "mts", "vob"};// 视频格式
            List<String> videoList = Arrays.asList(video);
            if (imgList.contains(suffix)) {
                map.put("flag", "img");//图片
            } else if (videoList.contains(suffix)) {
                map.put("flag", "video");//视频
            }
            map.put("src", src);
        }
        return listMap;
    }
}
