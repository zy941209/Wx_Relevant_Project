package cn.jsxz.common.utils.StringUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

/**
 * 自定义字符串处理工具类
 *
 * @author zy
 * @ClassName: StringUtil
 * @Description: TODO
 * @date 2018年7月24日
 */
public class StringUtil {

    /**
     * 过滤空NULL @Title: FilterNull @Description: TODO @param @param
     * o @param @return 参数 @return String 返回类型 @throws
     */
    public static String FilterNull(Object o) {
        return o != null && !"null".equals(o.toString()) ? o.toString().trim() : "";
    }

    /**
     * 是否为空 @Title: isEmpty @Description: TODO @param @param o @param @return
     * 参数 @return boolean 返回类型 @throws
     */
    public static boolean isEmpty(Object o) {
        if (o == null) {
            return true;
        }
        if ("".equals(FilterNull(o.toString()))) {
            return true;
        } else {
            return false;
        }
    }
    /**
     * 是否是数值
     *
     * @param str
     * @return
     */
    public static boolean isNumeric(String str) {
        Pattern pattern = Pattern.compile("[0-9]*");
        return pattern.matcher(str).matches();
    }

    /**
     * 是否不为空 @Title: isNotEmpty @Description: TODO @param @param
     * o @param @return 参数 @return boolean 返回类型 @throws
     */
    public static boolean isNotEmpty(Object o) {
        if (o == null) {
            return false;
        }
        if ("".equals(FilterNull(o.toString()))) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * 是否可转化为数字 @Title: isNum @Description: TODO @param @param o @param @return
     * 参数 @return boolean 返回类型 @throws
     */
    public static boolean isNum(Object o) {
        try {
            new BigDecimal(o.toString());
            return true;
        } catch (Exception e) {
        }
        return false;
    }

    /**
     * 从左截取固定长度字符串, 防止字符串超长, maxlength为0时默认50 @Title:
     * holdmaxlength @Description: TODO @param @param o @param @param
     * maxlength @param @return 参数 @return String 返回类型 @throws
     */
    public static String holdmaxlength(Object o, int maxlength) {
        maxlength = maxlength <= 0 ? 50 : maxlength;
        if (o == null) {
            return "";
        }
        return subStringByByte(o, maxlength);
    }

    /**
     * 按字节截取字符串 @Title: subStringByByte @Description: TODO @param @param
     * o @param @param len @param @return 参数 @return String 返回类型 @throws
     */
    private static String subStringByByte(Object o, int len) {
        if (o == null) {
            return "";
        }
        String str = o.toString();
        String result = null;
        if (str != null) {
            byte[] a = str.getBytes();
            if (a.length <= len) {
                result = str;
            } else if (len > 0) {
                result = new String(a, 0, len);
                int length = result.length();
                if (str.charAt(length - 1) != result.charAt(length - 1)) {
                    if (length < 2) {
                        result = null;
                    } else {
                        result = result.substring(0, length - 1);
                    }
                }
            }
        }
        return result;
    }

    /**
     * 逗号表达式_判断是否包含元素
     *
     * @Title: comma_contains @Description: TODO @param @param commaexpress
     * 逗号表达式 A,B,C @param @param element C @param @return 参数 @return boolean
     * 返回类型 true @throws
     */
    public static boolean comma_contains(String commaexpress, String element) {
        boolean flag = false;
        commaexpress = FilterNull(commaexpress);
        element = FilterNull(element);
        if (!"".equals(commaexpress) && !"".equals(element)) {
            String[] strlist = commaexpress.split(",");
            for (String str : strlist) {
                if (str.trim().equals(element.trim())) {
                    flag = true;
                    break;
                }
            }
        }
        return flag;
    }

    /**
     * 逗号表达式_添加
     *
     * @Title: comma_add @Description: TODO @param @param commaexpress 原逗号表达式 如
     * A,B @param @param newelement 新增元素 C @param @return 参数 @return String 返回类型
     * A,B,C @throws
     */
    public static String comma_add(String commaexpress, String newelement) {
        return comma_rect(FilterNull(commaexpress) + "," + FilterNull(newelement));
    }

    /**
     * 逗号表达式_删除
     *
     * @Title: comma_del @Description: TODO @param @param commaexpress 原逗号表达式 如
     * A,B,C @param @param delelement 删除元素 C,A @param @return 参数 @return String
     * 返回类型 B @throws
     */
    public static String comma_del(String commaexpress, String delelement) {
        if ((commaexpress == null) || (delelement == null) || (commaexpress.trim().equals(delelement.trim()))) {
            return "";
        }
        String[] deletelist = delelement.split(",");
        String result = commaexpress;
        for (String delstr : deletelist) {
            result = comma_delone(result, delstr);
        }
        return result;
    }

    /**
     * 逗号表达式_单一删除
     *
     * @Title: comma_delone @Description: TODO @param @param commaexpress 原逗号表达式
     * 如 A,B,C @param @param delelement 删除元素 C @param @return 参数 @return String
     * 返回类型 A,B @throws
     */
    public static String comma_delone(String commaexpress, String delelement) {
        if ((commaexpress == null) || (delelement == null) || (commaexpress.trim().equals(delelement.trim()))) {
            return "";
        }
        String[] strlist = commaexpress.split(",");
        StringBuffer result = new StringBuffer();
        for (String str : strlist) {
            if ((!str.trim().equals(delelement.trim())) && (!"".equals(str.trim()))) {
                result.append(str.trim() + ",");
            }
        }
        return result.toString().substring(0, result.length() - 1 > 0 ? result.length() - 1 : 0);
    }

    /**
     * 逗号表达式_取交集
     *
     * @Title: comma_intersect @Description: TODO @param @param commaexpressA
     * 逗号表达式1 A,B,C @param @param commaexpressB 逗号表达式2 B,C,D @param @return
     * 参数 @return String 返回类型 B,C @throws
     */
    public static String comma_intersect(String commaexpressA, String commaexpressB) {
        commaexpressA = FilterNull(commaexpressA);
        commaexpressB = FilterNull(commaexpressB);
        StringBuffer result = new StringBuffer();
        String[] strlistA = commaexpressA.split(",");
        String[] strlistB = commaexpressB.split(",");
        for (String boA : strlistA) {
            for (String boB : strlistB) {
                if (boA.trim().equals(boB.trim())) {
                    result.append(boA.trim() + ",");
                }
            }
        }
        return comma_rect(result.toString());
    }

    /**
     * 逗号表达式_反转
     *
     * @Title: comma_reverse @Description: TODO @param @param commaexpress
     * A,B,C @param @return 参数 @return String 返回类型 C,B,A @throws
     */
    public static String comma_reverse(String commaexpress) {
        commaexpress = FilterNull(commaexpress);
        String[] ids = commaexpress.split(",");
        StringBuffer str = new StringBuffer();
        for (int i = ids.length - 1; i >= 0; i--) {
            str.append(ids[i] + ",");
        }
        return comma_rect(str.toString());
    }

    /**
     * 逗号表达式_规范
     *
     * @Title: comma_rect @Description: TODO @param @param commaexpress 逗号表达式
     * ,A,B,B,,C @param @return 参数 @return String 返回类型 A,B,C @throws
     */
    public static String comma_rect(String commaexpress) {
        commaexpress = FilterNull(commaexpress);
        String[] strlist = commaexpress.split(",");
        StringBuffer result = new StringBuffer();
        for (String str : strlist) {
            if (!("".equals(str.trim())) && !("," + result.toString() + ",").contains("," + str + ",")
                    && !"null".equals(str)) {
                result.append(str.trim() + ",");
            }
        }
        return result.toString().substring(0, (result.length() - 1 > 0) ? result.length() - 1 : 0);
    }

    /**
     * 把异常信息转换成字符串，以方便保存
     *
     * @Title: getexceptionInfo @Description: TODO @param @param
     * e @param @return 参数 @return String 返回类型 @throws
     */
    public static String getexceptionInfo(Exception e) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            e.printStackTrace(new PrintStream(baos));
        } finally {
            try {
                baos.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        return baos.toString();
    }

    /**
     * 根据指定分割符，将字符串排序后返回
     *
     * @param strArr   需要分割排序的字符串
     * @param splitStr 指定分割符号（如：英文逗号','）
     * @return 排序后的字符串
     */
    public static String stringSort(String s, String splitStr) {
        String[] strArr = s.split(splitStr);
        if (null == strArr || strArr.length <= 0) {
            return null;
        }
        strArr = stringSort(strArr);
        String newStr = "";
        for (int i = 0; i < strArr.length; i++) {
            String strT = strArr[i];
            if (!"".equals(strT) && null != strT) {
                strT = strT.trim();
                if ("".equals(newStr)) {
                    newStr = strT;
                } else {
                    newStr += splitStr + strT;
                }
            }
        }
        return newStr;
    }

    /**
     * 字符串数组顺序排序
     *
     * @param s
     * @return
     */
    public static String[] stringSort(String[] s) {
        List<String> list = new ArrayList<String>(s.length);
        for (int i = 0; i < s.length; i++) {
            list.add(s[i]);
        }
        Collections.sort(list);
        return list.toArray(s);
    }

}
