package cn.jsxz.common.utils;

import cn.jsxz.common.constants.Constants;
import cn.jsxz.dto.Result;
import com.alibaba.fastjson.JSONObject;

/**
 * 返回对象工具类
 *
 * @author Admin
 * @ClassName: ResultUtil
 * @Description: TODO
 * @date 2018年7月23日
 */
public class ResultUtil {

    /* 成功有值 */
    public static Result success(Object object) {
        Result result = new Result();
        result.setCode(Constants.SUCCESS);
        result.setMsg(Constants.MSG_SUCCESS);
        result.setData(object);
        return result;
    }

    /* 成功无值 */
    public static Result success() {
        return success(null);
    }

    /* 失败 */
    public static Result error(String msg) {
        Result result = new Result();
        result.setCode(Constants.ERROR);
        result.setMsg(msg);
        return result;
    }

    /**
     * @auther: Zy
     * @method resultPage
     * 功能描述: 返回分页查询结果
     * @params: [count, object]
     * @return: * @return : com.alibaba.fastjson.JSONObject
     * @date: 2019-02-15 10:12:29
     */
    public static JSONObject resultPage(Long count, Object object) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code", 200);
        jsonObject.put("count", count);
        jsonObject.put("data", object);
        jsonObject.put("msg", "请求成功");
        return jsonObject;
    }

    /**
     * 返回JSON格式查询结果
     *
     * @param state   状态（int）
     * @param message 提示信息
     * @param object  对象
     * @return
     */
    public static JSONObject resultJSON(int state, String message, Object object) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code", state);
        jsonObject.put("msg", message);
        jsonObject.put("data", object);
        return jsonObject;
    }

    /**
     * @auther: Zy
     * @method jsonResult
     * 功能描述: 返回
     * @params: [code, message, object]
     * @return: * @return : cn.jsxz.dto.Result
     */
    public static Result jsonResult(int code, String message, Object object) {
        Result result = new Result();
        result.setMsg(message);
        result.setData(object);
        result.setCode(code);
        return result;
    }
}
