package cn.jsxz.common.utils;

import cn.hutool.setting.dialect.Props;

/**
 * 获取系统properties文件信息工具类
 *
 */
public class ReadPropertiesUtil {
    public String fileName;

    /**
     * 指定properties文件名，例:test(.properties后缀省略)
     *
     * @param fileName 文件名称
     */
    public ReadPropertiesUtil(String fileName) {
        this.fileName = fileName + ".properties";
    }

    /**
     * 根据key值去配置文件对于value
     *
     * @param key 指定键值
     * @return 对应key的值
     */
    public String getProValue(String key) {
        Props props = new Props(fileName, "utf-8");
        return props.getStr(key);
    }

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        ReadPropertiesUtil rpu = new ReadPropertiesUtil("qiniu");
        String QINIUAK = rpu.getProValue("QINIUAK");
        System.out.println(QINIUAK);
    }

}
