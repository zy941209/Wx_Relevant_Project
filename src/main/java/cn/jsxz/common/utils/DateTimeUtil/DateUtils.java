package cn.jsxz.common.utils.DateTimeUtil;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;


/**
 * 日期工具类
 *
 * @author wanglei
 */
public class DateUtils {

    private static int days; //天数
    private static int hours; //时
    private static int minutes; //分
    private static int seconds; //秒

    private static Logger logger = LoggerFactory.getLogger(DateUtils.class);

    /**
     * MM/dd
     */
    public static final String MONTH_DAY = "MM/dd";


    /**
     * yyyy.MM.dd
     */
    public static final String DATA_TIME_FORMAT_SPOT = "yyyy.MM.dd";


    /**
     * yyyy.MM.dd HH:mm
     */
    public static final String DATA_TIME_FORMAT_Minute = "yyyy.MM.dd HH:mm";

    /**
     * yyyy-MM-dd HH:mm:ss
     */
    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    /**
     * MM月dd日 HH时mm分
     */
    public static final String DATE_TIME_FORMAT_MM_STR = "MM月dd日 HH时mm分";

    /**
     * yyyy-MM-dd HH:mm:ss
     */
    public static final String DATE_TIME_FORMAT_MM = "yyyy-MM-dd HH:mm";

    /**
     * yyyy年MM月dd日 HH时mm分
     */
    public static final String DATE_TIME_FORMAT_String = "yyyy年MM月dd日 HH时mm分";

    /**
     * yyyyMMddHHmmss
     */
    public static final String DATE_TIME_FORMAT2 = "yyyyMMddHHmmss";
    /**
     * yyyyMMddHH
     */
    public static final String DATE_TIME_FORMAT_HOUR = "yyyyMMddHH";

    /**
     * yyyy-MM-dd HH
     */
    public static final String DATE_FORMAT_HOUR = "yyyy-MM-dd HH";

    /**
     * yyyy-MM-dd
     */
    public static final String DATE_FORMAT_DAY = "yyyy-MM-dd";

    /**
     * yyyy年-MM月-dd日
     */
    public static final String DATE_FORMAT_DAY_STR = "yyyy年MM月dd日";

    /**
     * yyyy-MM
     */
    public static final String DATE_FORMAT_MONTH = "yyyy-MM";

    /**
     * yyyy
     */
    public static final String DATE_FORMAT_YEAR = "yyyy";

    /**
     *
     */
    public static final String YEAR = "year";

    /**
     *
     */
    public static final String MONTH = "month";

    /**
     *
     */
    public static final String DAY = "day";

    /**
     *
     */
    public static final String HOUR = "hour";

    /**
     * 一天的毫秒数
     */
    public static final long SECOND = 86400000;

    /**
     * 得到当前日期 年-月-日
     *
     * @return String
     */
    public static String getCurrentDate() {
        return parse(new Date(), DATE_FORMAT_DAY);
    }

    /**
     * 得到当前日期d的开始 年-月-日
     *
     * @return String
     */
    public static String getCurrentDateStart() {
        String time = parse(new Date(), DATE_FORMAT_DAY);
        return time + " 00:00:00";
    }

    /**
     * 得到当前日期结束时间 年-月-日
     *
     * @return String
     */
    public static String getCurrentDateLast() {
        String time = parse(new Date(), DATE_FORMAT_DAY);
        return time + " 23:59:59";
    }

    /**
     * 得到当前时间 年-月-日  时:分:秒
     *
     * @return String
     */
    public static String getCurrentTime() {
        return parse(new Date(), DATE_TIME_FORMAT);
    }

    /**
     * 得到当前时间 年-月-日  时:分
     *
     * @return String
     */
    public static String getCurrentTimeMinute() {
        return parse(new Date(), DATE_TIME_FORMAT_MM);
    }

    /**
     * 得到当日的开始时间
     *
     * @return String
     */
    public static String getCurrentTime(String time) {
        return time + " 00:00:00";
    }

    /**
     * 得到当日的结束时间
     *
     * @return String
     */
    public static String getCurrentTimeUp(String time) {
        return time + " 23:59:59";
    }

    /**
     * 得到当前时间
     *
     * @return String
     */
    public static String getTimeCode() {
        return parse(new Date(), DATE_TIME_FORMAT2);
    }

    /**
     * 得到当前时间
     *
     * @return String
     */
    public static String getTimeCodeHour() {
        return parse(new Date(), DATE_TIME_FORMAT_HOUR);
    }

    /**
     * 得到当前时间年月
     *
     * @return String
     */
    public static String getCurrent_yyyy_MM() {
        return parse(new Date(), DATE_FORMAT_MONTH);
    }

    /**
     * 根据日期转成日期字符串(精确到秒)
     * yyyy-MM-dd HH:mm:ss
     *
     * @param date date
     * @return String
     */
    public static String parseDateTime(Date date) {
        return parse(date, DATE_TIME_FORMAT);
    }

    /**
     * 根据日期转成日期字符串(精确到秒)
     * yyyy-MM-dd HH:mm:ss
     *
     * @param date date
     * @return String
     */
    public static String parseDateTime(Date date, Integer type) {
        return type.equals(1) ? parse(date, DATE_FORMAT_DAY) + " 00:00:00" : parse(date, DATE_FORMAT_DAY) + " 23:59:59";
    }

    /**
     * 根据日期转成日期字符串(精确到秒)
     * yyyy-MM-dd HH:mm:ss
     *
     * @param date date
     * @return String
     */
    public static String parseTime(Date date) {
        return parse(date, DATE_FORMAT_DAY);
    }

    /**
     * 当前时间毫秒数
     *
     * @return <br>
     * <b>@author WangDong</b> <br>
     * 创建时间：2016年7月10日 下午12:25:19
     * @since 1.0
     */
    public static long currentTimeMillis() {
        Calendar c = Calendar.getInstance();
        return c.getTimeInMillis();
    }

    /**
     * 当前时间毫秒数+N+天后的毫秒数
     *
     * @return <br>
     * <b>@author WangDong</b> <br>
     * 创建时间：2016年7月10日 下午12:25:19
     * @since 1.0
     */
    public static long currentTimeMillisAddToDay(Integer day) {
        Calendar c = Calendar.getInstance();
        long msec = day * SECOND;
        return c.getTimeInMillis() + msec;
    }

    /**
     * @param date 时间
     * @return <br><b>@author WangDong</b>
     * <br>创建时间：2016年9月19日 上午1:47:32
     * @since 1.0
     */
    public static String formatYYYYMMDDStr(Date date) {
        if (date == null) {
            return null;
        }
        return parse(date, DATE_FORMAT_DAY);
    }

    /**
     * 获取两个日期之间的时间间隔标识，只精确到小时
     *
     * @param beginTime beginTime
     * @param endTime   endTime
     * @return month/day/hour
     */
    public static String getTimeIntervalName(String beginTime, String endTime) {
        Date beginDate = parseDateTime_MM(beginTime);
        Date endDate = parseDateTime_MM(endTime);
        long intervalTime = endDate.getTime() - beginDate.getTime();
        if (intervalTime == 0 || intervalTime < 0) {
            return "您选择的时间范围不正确";
        }
        if (beforeByDateField(Calendar.YEAR, beginDate, endDate)) {
            return YEAR;
        } else if (beforeByDateField(Calendar.MONTH, beginDate, endDate)) {
            return MONTH;
        } else if (beforeByDateField(Calendar.DATE, beginDate, endDate)) {
            return DAY;
        } else {
            return HOUR;
        }
    }

    /**
     * 获取两个日期之间的时间间隔标识，只精确到小时
     *
     * @param beginTime beginTime
     * @param endTime   endTime
     * @return month/day/hour
     */
    public static Integer getDays(String beginTime, String endTime) {
//		if(StringUtils.isEmpty(beginTime)){
//			return 0;
//		}
        Date beginDate = parseDate(beginTime);
        Date endDate = parseDate(endTime);
        Long time = endDate.getTime() - beginDate.getTime();
        Long dayTime = time / 1000 / 60 / 60 / 24;//天
        return new Integer(dayTime.intValue());
    }

    /**
     * 获取两个日期之间的时间间隔标识，只精确到分钟
     *
     * @param beginTime beginTime
     * @param endTime   endTime
     * @return month/day/hour
     */
    public static Integer getMinute(String beginTime, String endTime) {
        Date beginDate = parseDateTime(beginTime);
        Date endDate = parseDateTime(endTime);
        Long time = endDate.getTime() - beginDate.getTime();
        Long dayTime = time / 1000 / 60;//天
        return new Integer(dayTime.intValue());
    }

    /**
     * 获取两个日期之间的时间间隔标识，只精确到分钟
     *
     * @param beginTime beginTime
     * @param endTime   endTime
     * @return month/day/hour
     */
    public static Integer getMinute2(String beginTime, String endTime) {
        Date beginDate = parseDateTime_MM(beginTime);
        Date endDate = parseDateTime_MM(endTime);
        Long time = endDate.getTime() - beginDate.getTime();
        Long dayTime = time / 1000 / 60;//天
        return new Integer(dayTime.intValue());
    }

    /**
     * 根据日期间隔的标识得到间隔的日期数组
     *
     * @param beginTime beginTime
     * @param endTime   endTime
     * @return String[]
     */
    public static String[] getBetweenDayArr(String beginTime, String endTime) {
        String intervalName = getTimeIntervalName(beginTime, endTime);
        Date beginDate = parseDateTime(beginTime);
        Date endDate = parseDateTime(endTime);
        List<String> list = new ArrayList<String>();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(beginDate);
        while (calendar.getTime().getTime() <= endDate.getTime()) {
            if (HOUR.equals(intervalName)) {
                SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_HOUR);
                calendar.set(Calendar.MINUTE, 0);
                list.add(sdf.format(calendar.getTime()));
                calendar.add(Calendar.HOUR, 1);
            } else if (DAY.equals(intervalName)) {
                SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_DAY);
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                list.add(sdf.format(calendar.getTime()));
                calendar.add(Calendar.DATE, 1);
            } else if (MONTH.equals(intervalName)) {
                SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_MONTH);
                calendar.set(Calendar.DAY_OF_MONTH, 1);
                list.add(sdf.format(calendar.getTime()));
                calendar.add(Calendar.MONTH, 1);
            } else {
                SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_YEAR);
                calendar.set(Calendar.DAY_OF_MONTH, 1);
                calendar.set(Calendar.MONTH, 0);
                list.add(sdf.format(calendar.getTime()));
                calendar.add(Calendar.YEAR, 1);
            }
        }
        return list.toArray(new String[]{});
    }


    /**
     * 获取当前Timestamp 时间
     *
     * @return <br>
     * <b>@author WangDong</b> <br>
     * 创建时间：2016年8月7日 下午1:23:31
     * @since 1.0
     */
    public static Timestamp currentTimestemp() {
        Calendar c = Calendar.getInstance();
        return new Timestamp(c.getTimeInMillis());
    }

    /**
     * 日期字符串转换精确到秒
     *
     * @param dateTime datetime
     * @return Date
     */
    public static Date parseDateTime(String dateTime) {
        return parse(dateTime, DATE_TIME_FORMAT);
    }

    /**
     * 日期字符串转换精确到秒
     *
     * @param dateTime datetime
     * @return Date
     */
    public static Date parseDateTime_MM(String dateTime) {
        return parse(dateTime, DATE_TIME_FORMAT_MM);
    }


    /**
     * date增加小时
     *
     * @param hour
     * @return Date
     * @Description: TODO
     * @author 尚念鑫
     * @date 2017-12-11下午1:55:57
     */
    public static String addDateHour(int hour) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.HOUR, (int) hour);
        return parseDateTime(cal.getTime());
    }


    /**
     * date增加小时
     *
     * @param hour
     * @return Date
     * @Description: TODO
     * @author 尚念鑫
     * @date 2017-12-11下午1:55:57
     */
    public static String subDateHour(int hour) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.HOUR, -hour);
        return parseDateTime(cal.getTime());
    }


    /**
     * 日期字符串转换,精确到天
     *
     * @param date date
     * @return Date
     */
    public static Date parseDate(String date) {
        return parse(date, DATE_FORMAT_DAY);
    }

    /**
     * 根据日期字符串转换成日期格式
     *
     * @param date   date
     * @param format format
     * @return Date
     */
    public static Date parse(String date, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        try {
            return sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            try {
                throw new Exception("日期格式不正确");
            } catch (Exception e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
        }
        return null;
    }

    /**
     * 根据日期对象转换成日期字符
     *
     * @param date   date
     * @param format format
     * @return String
     */
    public static String parse(Date date, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(date);
    }


    /**
     * 转换日期格式
     *
     * @param time      date
     * @param oldFormat format
     * @return String
     */
    public static String formatTime(String time, String oldFormat, String newFormat) {
        String formatTime = "";
        SimpleDateFormat sdf = new SimpleDateFormat(oldFormat);
        Date date = null;
        try {
            date = sdf.parse(time);
            formatTime = new SimpleDateFormat(newFormat).format(date);
        } catch (ParseException ex) {
            return formatTime;
        }
        return formatTime;
    }

    /**
     * @param date date
     * @return <br><b>作者： @author WangDong</b>
     * <br>创建时间：2016年11月18日 上午12:58:30
     * @since 1.0
     */
    public static String getYear(Date date) {
        return getByField(date, Calendar.YEAR);
    }

    /**
     * @param date date
     * @return <br><b>作者： @author WangDong</b>
     * <br>创建时间：2016年11月18日 上午12:58:32
     * @since 1.0
     */
    public static String getMonth(Date date) {
        return getByField(date, Calendar.MONTH);
    }

    /**
     * @param date date
     * @return <br><b>作者： @author WangDong</b>
     * <br>创建时间：2016年11月18日 上午12:58:35
     * @since 1.0
     */
    public static String getDay(Date date) {
        return getByField(date, Calendar.DAY_OF_MONTH);
    }

    /**
     * @param date date
     * @return <br><b>作者： @author WangDong</b>
     * <br>创建时间：2016年11月18日 上午12:58:38
     * @since 1.0
     */
    public static String getHour(Date date) {
        return getByField(date, Calendar.HOUR_OF_DAY);
    }

    /**
     * @param date date
     * @return <br><b>作者： @author WangDong</b>
     * <br>创建时间：2016年11月18日 上午12:58:40
     * @since 1.0
     */
    public static String getMinute(Date date) {
        return getByField(date, Calendar.MINUTE);
    }

    /**
     * @param date date
     * @return <br><b>作者： @author WangDong</b>
     * <br>创建时间：2016年11月18日 上午12:58:43
     * @since 1.0
     */
    public static String getSecond(Date date) {
        return getByField(date, Calendar.SECOND);
    }

    /**
     * 根据字段获取日期字段信息
     *
     * @param date
     * @param field
     * @return
     */
    private static String getByField(Date date, int field) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int result = 0;
        if (Calendar.MONTH == field) {
            result = (calendar.get(field) + 1);
        } else {
            result = calendar.get(field);
        }
        if (result < 10) {
            return "0" + result;
        }
        return "" + result;
    }

    /**
     * 根据日期字段判断日期是否在日期字段区间,默认为1
     *
     * @param dateField dateField
     * @param beginDate beginDate
     * @param endDate   endDate
     * @return booelan
     */
    public static boolean beforeByDateField(int dateField, Date beginDate,
                                            Date endDate) {
        return beforeByDateField(dateField, 1, beginDate, endDate);
    }

    /**
     * 根据日期字段和日期数判断日期是否在日期字段数区间
     *
     * @param dateField  dateField
     * @param dateNumber dateNumber
     * @param beginDate  beginDate
     * @param endDate    endDate
     * @return boolean
     */
    public static boolean beforeByDateField(int dateField, int dateNumber,
                                            Date beginDate, Date endDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(beginDate);
        calendar.add(dateField, dateNumber);
        return calendar.getTime().before(endDate);
    }

    /**
     * 获取num天之后的日期
     *
     * @param
     * @return String
     */
    public static String getLaterDayDate(int days) {
        Calendar now = Calendar.getInstance();
        now.set(Calendar.DATE, now.get(Calendar.DATE) + days);
        return parseDateTime(now.getTime());

    }

    /**
     * * 获得指定日期的前一天
     */
    public static String getDayBefore(String before) {
        @SuppressWarnings("unused")
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        Date date = null;
        try {
            date = new SimpleDateFormat("yy-MM-dd").parse(before);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.setTime(date);
        int day = c.get(Calendar.DATE);
        c.set(Calendar.DATE, day - 1);
        String dayBefore = new SimpleDateFormat("yyyy-MM-dd").format(c.getTime());
        return dayBefore;
    }


    /**
     * 获得指定日期的后一天 返回年月日
     *
     * @param
     * @return
     */
    public static String getDayAfter(String after) {
        Calendar c = Calendar.getInstance();
        Date date = null;
        try {
            date = new SimpleDateFormat("yy-MM-dd").parse(after);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.setTime(date);
        int day = c.get(Calendar.DATE);
        c.set(Calendar.DATE, day + 1);
        String dayAfter = new SimpleDateFormat("yyyy-MM-dd").format(c.getTime());
        return dayAfter;
    }


    /**
     * 获得指定日期的后一天  返回年月日  时分秒
     *
     * @param
     * @return
     */
    public static String getDayAfters(String after) {
        Calendar c = Calendar.getInstance();
        Date date = null;
        try {
            date = new SimpleDateFormat("yy-MM-dd").parse(after);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.setTime(date);
        int day = c.get(Calendar.DATE);
        c.set(Calendar.DATE, day + 1);
        String dayAfter = new SimpleDateFormat("yyyy-MM-dd HH:ss:mm").format(c.getTime());
        return dayAfter;
    }

    /**
     * 获取num天之前的日期
     *
     * @param
     * @return String
     */
    public static String getBeforeDayDate(int days, int type) {
        Calendar now = Calendar.getInstance();
        now.set(Calendar.DATE, now.get(Calendar.DATE) - days);
        return parseDateTime(now.getTime(), type);
    }

    /**
     * 获取num天之前的日期
     *
     * @param
     * @return String
     */
    public static String getBeforeDayDate(int days) {
        Calendar now = Calendar.getInstance();
        now.set(Calendar.DATE, now.get(Calendar.DATE) - days);
        return parseDateTime(now.getTime());
    }

    /**
     * 获取num天之后的日期
     *
     * @param
     * @return String
     */
    public static String getAfterDayDate(int days) {
        Calendar now = Calendar.getInstance();
        now.set(Calendar.DATE, now.get(Calendar.DATE) + days);
        return parseTime(now.getTime());
    }

    /**
     * date 时间 增加 day 天
     *
     * @param date 时间
     * @param day  天数
     * @return <br>
     * <b>@author WangDong</b> <br>
     * 创建时间：2016年8月11日 下午12:54:56
     * @since 1.0
     */
    public static Date addDate(Date date, int day) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_MONTH, day);
        return cal.getTime();
    }


    /**
     * date 时间 增加 seconds 秒
     *
     * @param date    时间
     * @param seconds 天数
     * @return <br>
     * <b>@author WangDong</b> <br>
     * 创建时间：2016年8月11日 下午12:54:56
     * @since 1.0
     */
    public static Date addDateSecond(Date date, long seconds) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.SECOND, (int) seconds);
        return cal.getTime();
    }

    /**
     * 获得几分钟之前的时间
     *
     * @param minute
     * @return
     */
    public static String addDateMinute(int minute) {
        Calendar nowTime2 = Calendar.getInstance();
        nowTime2.add(Calendar.MINUTE, -minute);//30分钟前的时间
        return parse(nowTime2.getTime(), DATE_TIME_FORMAT);
    }

    /**
     * 获得几分钟之前的时间
     *
     * @param minute
     * @return
     */
    public static String addDateMinute_MM(int minute) {
        Calendar nowTime2 = Calendar.getInstance();
        nowTime2.add(Calendar.MINUTE, -minute);//30分钟前的时间
        return parse(nowTime2.getTime(), DATE_TIME_FORMAT_MM);
    }

    /**
     * 获得几分钟之前的时间
     *
     * @param minute
     * @return
     */
    public static String addDateMinute_MM(String time, int minute) {
        Calendar nowTime2 = Calendar.getInstance();
        nowTime2.setTime(parse(time, DATE_TIME_FORMAT_MM));
        nowTime2.add(Calendar.MINUTE, -minute);//30分钟前的时间
        return parse(nowTime2.getTime(), DATE_TIME_FORMAT_MM);
    }

    /**
     * 获得几分钟之后的时间
     *
     * @param minute
     * @return
     */
    public static String jianDateMinute(int minute) {
        Calendar nowTime = Calendar.getInstance();
        nowTime.add(Calendar.MINUTE, minute);//30分钟后的时间
        return parse(nowTime.getTime(), DATE_TIME_FORMAT);
    }

    /**
     * 获得几分钟之后的时间
     *
     * @param minute
     * @return
     */
    public static String jianDateMinute_MM(int minute) {
        Calendar nowTime = Calendar.getInstance();
        nowTime.add(Calendar.MINUTE, minute);//30分钟后的时间
        return parse(nowTime.getTime(), DATE_TIME_FORMAT_MM);
    }

    /**
     * 获得几分钟之后的时间
     *
     * @param minute
     * @return
     */
    public static String jianDateMinute_MM(String time, int minute) {
        Calendar nowTime = Calendar.getInstance();
        nowTime.setTime(parse(time, DATE_TIME_FORMAT_MM));
        nowTime.add(Calendar.MINUTE, minute);//30分钟后的时间
        return parse(nowTime.getTime(), DATE_TIME_FORMAT_MM);
    }

    /**
     * date增加分钟
     *
     * @param date
     * @param minute
     * @return Date
     * @Description: TODO
     * @author 尚念鑫
     * @date 2017-12-11下午1:54:56
     */
    public static Date addDateMinute(Date date, int minute) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MINUTE, (int) minute);
        return cal.getTime();
    }

    /**
     * date增加小时
     *
     * @param date
     * @param hour
     * @return Date
     * @Description: TODO
     * @author 尚念鑫
     * @date 2017-12-11下午1:55:57
     */
    public static Date addDateHour(Date date, int hour) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.HOUR, (int) hour);
        return cal.getTime();
    }


    /**
     * 获取 num月之前的日期
     *
     * @param num num
     * @return String
     */
    public static String getBeforeMonthDate(int num) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, num);
        return parseDateTime(cal.getTime());

    }

    /**
     * 获取所有整点
     *
     * @return String[]
     */
    public static String[] getBetweenHourArr() {
        List<String> list = new ArrayList<String>();
        for (int i = 0; i < 24; i++) {
            String j = "";
            if (i < 10) {
                j = "0" + i;
            } else {
                j = i + "";
            }
            list.add(j + ":00");

        }
        return list.toArray(new String[]{});

    }

    /**
     * 只获取日期
     *
     * @param beginTime beginTime
     * @param endTime   endTime
     * @return String[]
     */
    public static String[] getBetweenDay(String beginTime, String endTime) {
        Date beginDate = parseDateTime(beginTime);
        Date endDate = parseDateTime(endTime);
        List<String> list = new ArrayList<String>();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(beginDate);
        while (calendar.getTime().getTime() <= endDate.getTime()) {
            SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_DAY);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            list.add(sdf.format(calendar.getTime()));
            calendar.add(Calendar.DATE, 1);
        }
        return list.toArray(new String[]{});
    }

    /**
     * 获取某一日期后固定天数后的日期
     *
     * @param date date
     * @param days days
     * @return <br><b>作者： @author WangDong</b>
     * <br>创建时间：2016年12月14日 下午7:59:42
     * @since 1.0
     */
    public static Date getAfterByDate(Date date, int days) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, days);
        return calendar.getTime();
    }


    /**
     * 获取某一日期后固定天数后的日期
     *
     * @param time time
     * @param days days
     * @return <br><b>作者： @author WangDong</b>
     * <br>创建时间：2016年12月14日 下午7:59:42
     * @since 1.0
     */
    public static String getAfterByDate(String time, int days) {
        if (time.length() == 10) {
            String nowTime = getCurrentTime();
            time += " " + nowTime.substring(11, nowTime.length());
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = format.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, days);
        return parse(calendar.getTime(), DATE_TIME_FORMAT);
    }

    /**
     * 校验是否为符合规则的日期
     *
     * @param date       date
     * @param dateFormat dateFormat
     * @return <br><b>作者： @author WangDong</b>
     * <br>创建时间：2016年8月9日 下午5:47:41
     * @since 1.0
     */
    public static boolean checkDate(String date, String dateFormat) {
        SimpleDateFormat format = new SimpleDateFormat(dateFormat);
        try {
            // 设置lenient为false. 否则SimpleDateFormat会比较宽松地验证日期，比如2007/02/29会被接受，并转换成2007/03/01
            format.setLenient(false);
            format.parse(date);
        } catch (Exception e) {
            // 如果throw java.text.ParseException或者NullPointerException，就说明格式不对
            logger.error(e.getMessage());
            return false;
        }
        return true;
    }


    /**
     * 两个时间之间相差距离多少天
     *
     * @param startTime 开始时间 （小）
     * @param endTime   结束时间（大）
     * @return
     * @throws Exception <br><b>作者： @author WangDongdong</b>
     *                   <br>创建时间：2017-6-8 上午11:03:52
     * @since 1.0
     */
    public static Integer getDistanceDays(String startTime, String endTime) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date one;
        Date two;
        Long days = 0L;
        try {
            one = df.parse(startTime);
            two = df.parse(endTime);
            Long time1 = one.getTime();
            Long time2 = two.getTime();
            Long diff;
            if (time1 < time2) {
                diff = time2 - time1;
            } else {
                diff = time1 - time2;
            }
            days = (diff / (1000 * 60 * 60 * 24));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Integer(days.intValue());
    }

    /**
     * 两个时间相差距离多少天多少小时多少分多少秒
     *
     * @param str1 时间参数 1 格式：1990-01-01 12:00:00
     * @param str2 时间参数 2 格式：2009-01-01 12:00:00
     * @return long[] 返回值为：{天, 时, 分, 秒}
     */
    public static long[] getDistanceTimes(String str1, String str2) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date one;
        Date two;
        long day = 0;
        long hour = 0;
        long min = 0;
        long sec = 0;
        try {
            one = df.parse(str1);
            two = df.parse(str2);
            long time1 = one.getTime();
            long time2 = two.getTime();
            long diff;
            if (time1 < time2) {
                diff = time2 - time1;
            } else {
                diff = time1 - time2;
            }
            day = diff / (24 * 60 * 60 * 1000);
            hour = (diff / (60 * 60 * 1000) - day * 24);
            min = ((diff / (60 * 1000)) - day * 24 * 60 - hour * 60);
            sec = (diff / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long[] times = {day, hour, min, sec};
        return times;
    }

    /**
     * 两个时间相差距离多少秒
     * str1 小于str2 返回负数
     *
     * @param str1 时间参数 1 格式：1990-01-01 12:00:00
     * @param str2 时间参数 2 格式：2009-01-01 12:00:00
     * @return long[] 返回值为：{天, 时, 分, 秒}
     */
    public static Integer getSecond(String str1, String str2) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date one;
        Date two;
        long day = 0;
        long hour = 0;
        long min = 0;
        long sec = 0;
        try {
            one = df.parse(str1);
            two = df.parse(str2);
            long time1 = one.getTime();
            long time2 = two.getTime();
            long diff = time1 - time2;
            day = diff / (24 * 60 * 60 * 1000);
            hour = (diff / (60 * 60 * 1000) - day * 24);
            min = ((diff / (60 * 1000)) - day * 24 * 60 - hour * 60);
            sec = (diff / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return (int) sec;
    }

    /**
     * 两个时间相差距离多少天多少小时多少分多少秒
     *
     * @param str1 时间参数 1 格式：1990-01-01 12:00:00
     * @param str2 时间参数 2 格式：2009-01-01 12:00:00
     * @return String 返回值为：xx天xx小时xx分xx秒
     */
    public static String getDistanceTime(String str1, String str2) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date one;
        Date two;
        long day = 0;
        long hour = 0;
        long min = 0;
        long sec = 0;
        try {
            one = df.parse(str1);
            two = df.parse(str2);
            long time1 = one.getTime();
            long time2 = two.getTime();
            long diff;
            if (time1 < time2) {
                diff = time2 - time1;
            } else {
                diff = time1 - time2;
            }
            day = diff / (24 * 60 * 60 * 1000);
            hour = (diff / (60 * 60 * 1000) - day * 24);
            min = ((diff / (60 * 1000)) - day * 24 * 60 - hour * 60);
            sec = (diff / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return day + "天" + hour + "小时" + min + "分" + sec + "秒";
    }


    /**
     * 两个时间相差距离多少天多少小时多少分
     *
     * @param str1 时间参数 1 格式：1990-01-01 12:00:00
     * @param str2 时间参数 2 格式：2009-01-01 12:00:00
     * @return String 返回值为：xx天xx小时xx分xx秒
     */
    public static String getDistanceTime2(String str1, String str2) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date one;
        Date two;
        long day = 0;
        long hour = 0;
        long min = 0;
        try {
            one = df.parse(str1);
            two = df.parse(str2);
            long time1 = one.getTime();
            long time2 = two.getTime();
            long diff;
            if (time1 < time2) {
                diff = time2 - time1;
            } else {
                diff = time1 - time2;
            }
            day = diff / (24 * 60 * 60 * 1000);
            hour = (diff / (60 * 60 * 1000) - day * 24);
            min = ((diff / (60 * 1000)) - day * 24 * 60 - hour * 60);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return day + "天" + hour + "小时" + min + "分";
    }


    /**
     * 获取当月第一天
     *
     * @return <br><b>作者： @author WangDongdong</b>
     * <br>创建时间：2017-6-27 下午7:41:45
     * @since 1.0
     */
    public static String getfirstDay() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, 0);
        c.set(Calendar.DAY_OF_MONTH, 1);//设置为1号,当前日期既为本月第一天
        return format.format(c.getTime());
    }

    /**
     * 获取当月最后一天
     *
     * @return <br><b>作者： @author WangDongdong</b>
     * <br>创建时间：2017-6-27 下午7:41:45
     * @since 1.0
     */
    public static String getlastDay() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Calendar ca = Calendar.getInstance();
        ca.set(Calendar.DAY_OF_MONTH, ca.getActualMaximum(Calendar.DAY_OF_MONTH));
        return format.format(ca.getTime());
    }


    /**
     * 获取指定日期月份最后天
     *
     * @param
     * @return
     * @throws Exception <br><b>作者： @author WangDongdong</b>
     *                   <br>创建时间：2017-6-28 上午11:42:36
     * @since 1.0
     */
    public static String getlastDay(String time) {
        Date date = null;
        String day_last = null;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            date = format.parse(time);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        //创建日历
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, 1);    //加一个月
        calendar.set(Calendar.DATE, 1);     //设置为该月第一天
        calendar.add(Calendar.DATE, -1);    //再减一天即为上个月最后一天
        day_last = format.format(calendar.getTime());
        return day_last;
    }


    /**
     * 获取指定日期月份第一天
     *
     * @param
     * @return
     * @throws Exception <br><b>作者： @author WangDongdong</b>
     *                   <br>创建时间：2017-6-28 上午11:43:06
     * @since 1.0
     */
    public static String getfirstDay(String time) {
        Date date = null;
        String day_first = null;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            date = format.parse(time);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        //创建日历
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        day_first = format.format(calendar.getTime());
        return day_first;
    }


    /**
     * 获取指定日期月份第一天
     * 带有时分秒
     *
     * @param time
     * @return <br><b>作者： @author WangDong</b>
     * <br>创建时间：2017-12-13 下午2:24:08
     * @since 1.0
     */
    public static String getfirstDayTime(String time) {
        return time.substring(0, 7) + "-01 00:00:00";
    }

    /**
     * 获取指定日期月份
     * 带有时分秒
     *
     * @param time
     * @return <br><b>作者： @author WangDong</b>
     * <br>创建时间：2017-12-13 下午2:24:08
     * @since 1.0
     */
    public static String getMonth(String time) {
        return time.substring(0, 7);
    }


    /**
     * 获取指定日期月份最后一天
     * 带有时分秒
     *
     * @param time
     * @return <br><b>作者： @author WangDong</b>
     * <br>创建时间：2017-12-13 下午2:26:16
     * @since 1.0
     */
    public static String getlastDayTime(String time) {
        return time.substring(0, 7) + "-31 23:59:59";
    }

    /**
     * 在时间的基础上添加xx年
     *
     * @param time
     * @param years
     * @return <br><b>作者： @author WangDong</b>
     * <br>创建时间：2017-10-18 上午10:14:39
     */
    @SuppressWarnings("static-access")
    public static String addYearsTime(String time, Integer years) {
        Calendar calendar = new GregorianCalendar();
        Date parseDate = new Date(System.currentTimeMillis());
        if (StringUtils.isNotBlank(time)) {
            parseDate = parseDate(time);
        }
        calendar.setTime(parseDate);
        calendar.add(calendar.YEAR, years);//把日期往后增加一年.整数往后推,负数往前移动
        parseDate = calendar.getTime();
        String parse = parse(parseDate, "yyyy-MM-dd");
        return parse;
    }


    /**
     * 格式化日期
     *
     * @param date 日期对象
     * @return String 日期字符串
     */
    public static String formatDate(Date date) {
        SimpleDateFormat f = new SimpleDateFormat(DATE_FORMAT_DAY);
        String sDate = f.format(date);
        return sDate;
    }

    public static String formatDatehMs(Date date) {
        SimpleDateFormat f = new SimpleDateFormat(DATE_FORMAT_DAY);
        String sDate = f.format(date);
        return sDate;
    }

    /**
     * 获取当年的第一天
     *
     * @param
     * @return
     */
    public static String getCurrYearFirst() {
        Calendar currCal = Calendar.getInstance();
        int currentYear = currCal.get(Calendar.YEAR);
        return getYearFirst(currentYear);
    }

    /**
     * 获取当年的最后一天
     *
     * @param
     * @return
     */
    public static String getCurrYearLast() {
        Calendar currCal = Calendar.getInstance();
        int currentYear = currCal.get(Calendar.YEAR);
        return getYearLast(currentYear);
    }

    /**
     * 获取某年第一天日期
     *
     * @param year 年份
     * @return Date
     */
    public static String getYearFirst(int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.YEAR, year);
        Date currYearFirst = calendar.getTime();
        return formatDate(currYearFirst);
    }

    /**
     * 获取某年第一天日期  带有时分秒
     *
     * @param
     * @return Date
     */
    public static String getYearFirst(String time) {
        return time.substring(0, 4) + "-01-01 00:00:00";
    }

    /**
     * 获取某年最后一天日期  带有时分秒
     *
     * @param
     * @return Date
     */
    public static String getYearLast(String time) {
        return time.substring(0, 4) + "-12-31 23:59:59";
    }

    /**
     * 获取某年最后一天日期
     *
     * @param year 年份
     * @return Date
     */
    public static String getYearLast(int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.YEAR, year);
        calendar.roll(Calendar.DAY_OF_YEAR, -1);
        Date currYearLast = calendar.getTime();

        return formatDate(currYearLast);
    }

    public static Integer getYear() {
        Calendar now = Calendar.getInstance();
        return now.get(Calendar.YEAR);
    }

    public static Integer getMonth() {
        Calendar now = Calendar.getInstance();
        return now.get(Calendar.MONTH) + 1;
    }

    /**
     * 两日期时间差，
     *
     * @return Long[]
     * @Description: TODO
     * @author 尚念鑫
     * @date 2017-12-13上午11:32:26
     */
    public static Long[] dateMarth(String datebegin, String dateend) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long diff = 0;
        long days = 0;
        long hours = 0;
        long minutes = 0;
        long miao = 0;
        try {
            Date d1 = df.parse(dateend);   //结束时间
            Date d2 = df.parse(datebegin); //开始时间
            diff = d1.getTime() - d2.getTime();//这样得到的差值是微秒级别
            days = diff / (1000 * 60 * 60 * 24);

            hours = (diff - days * (1000 * 60 * 60 * 24)) / (1000 * 60 * 60);
            minutes = (diff - days * (1000 * 60 * 60 * 24) - hours * (1000 * 60 * 60)) / (1000 * 60);
            miao = (diff - days * (1000 * 60 * 60 * 24) - hours * (1000 * 60 * 60) - minutes * (1000 * 60)) / 1000;
            System.out.println("" + days + "天" + hours + "小时" + minutes + "分" + miao + "秒");
        } catch (Exception e) {
        }
        return new Long[]{days, hours, minutes, miao};
    }

    public static Long dateMathTime(String datebegin, String dateend) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long diff = 0;
        Date d1;
        Date d2;
        try {
            d1 = df.parse(dateend);
            d2 = df.parse(datebegin); //开始时间
            diff = d1.getTime() - d2.getTime();//这样得到的差值是微秒级别
        } catch (ParseException e) {
            // TODO 自动生成的 catch 块
            e.printStackTrace();
        }   //结束时间

        return diff / 1000;
    }

    /**
     * 得到当前时间+随机
     *
     * @return String
     */
    public static String getTime() {
//		String valueOf = String.valueOf(System.currentTimeMillis());
//		 String parse = parse(new Date(), DATE_TIME_FORMAT2);
//		 StringBuffer  str = new StringBuffer("L"+valueOf.substring(0,3));
//		 str.append(parse);
//		 str.append(valueOf.substring(valueOf.length()-3, valueOf.length()));
        String parse = parse(new Date(), DATE_TIME_FORMAT2);
        StringBuffer str = new StringBuffer("L");
        str.append(parse);
        // str.append(valueOf.substring(valueOf.length()-3, valueOf.length()));
        return str.toString();
    }


    /**
     * 创建订单号
     *
     * @param code 订单前置标识  微信 ：WX  支付宝：ZFB
     * @return <br><b>作者： @author WangDong</b>
     * <br>创建时间：2018-1-3 下午2:13:14
     * @since 1.0
     */
    public static String getOrderCode(String code) {
        code = code.toUpperCase();
        String parse = parse(new Date(), DATE_TIME_FORMAT2);
        StringBuffer str = new StringBuffer(code + parse);
        str.append(String.valueOf(System.currentTimeMillis()));
        return str.toString();
    }

    public static String formatTime(String time, int type) {
        if (StringUtils.isEmpty(time)) {
            return "";
        }
        if (time.length() == 16) {
            time += ":00";
        }
        String s = time.replaceFirst("-", "年").replaceFirst("-", "月").replaceFirst(" ", "日 ").
                replaceFirst(":", "时").replaceFirst(":", "分");
        if (type == 1) {
            return s.substring(0, 11);
        }
        return s.substring(0, 18);
    }

    public static void main(String[] args) {

//        String[] betweenDayArr = getBetweenDayArr("2018-04-02 23:59:59","2018-05-02 00:00:00");
//        System.out.println(betweenDayArr);
        Integer s = getMinute2("2018-07-19 13:00:00", "2018-07-19 15:00:00");
        //String s = addDateMinute_MM("2018-07-19 09:00",5);
        //String s1 = jianDateMinute_MM("2018-07-19 09:00",5);
        //String s2 = getLaterDayDate(5);
        //Integer distanceTimes = getSecond("2018-07-19 09:00:00", "2018-07-19 09:00:05");
        System.out.println(s);
        //System.out.println(s1);
        //System.out.println(s2);
        // System.out.println(distanceTimes.toString());
    }
}
