package cn.jsxz.common.utils.ImageUtil;


import com.itextpdf.forms.PdfAcroForm;
import com.itextpdf.forms.fields.PdfFormField;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.border.SolidBorder;
import com.itextpdf.layout.element.Paragraph;
import org.junit.Test;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class itext7Utils {
    /*     * 生成一个简单的pdf文件
     *
     */
    @Test
    public void createPdf() {
        // 生成的新文件路径
        /*String fileName = StringExtend.format("itext-pdf-{0}.pdf", DateExtend.getDate("yyyyMMddHHmmss"));
        String newPDFPath = PathExtend.Combine("D:/Temp/pdf/", fileName);*/

        String fileName = "";
        String newPDFPath = "";
        try {
            //处理中文问题
            PdfFont font = PdfFontFactory.createFont("STSongStd-Light", "UniGB-UCS2-H", false);
            PdfWriter writer = new PdfWriter(new FileOutputStream(newPDFPath));
            //Initialize PDF document
            PdfDocument pdf = new PdfDocument(writer);
            Document document = new Document(pdf);

            Paragraph p = new Paragraph("你好,世界！hello word!");
            p.setFont(font);
            p.setFontSize(12);
            p.setBorder(new SolidBorder(Color.BLACK, 0.5f));
            document.add(p);

            document.close();
            writer.close();
            pdf.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 使用pdf 模板生成 pdf 文件
     */
    public static String fillTemplate(String realPath, String mobanPath, String orderNo, HashMap map) {// 利用模板生成pdf
        // 模板路径
        String templatePath = mobanPath;
        // 生成的新文件路径
        String newPDFPath = realPath + "/PDF/" + orderNo + ".pdf";
        try {
            //Initialize PDF document
            PdfDocument pdf = new PdfDocument(new PdfReader(templatePath), new PdfWriter(newPDFPath));
            PdfAcroForm form = PdfAcroForm.getAcroForm(pdf, true);
            Map<String, PdfFormField> fields = form.getFormFields();

            //处理中文问题
            PdfFont font = PdfFontFactory.createFont("STSongStd-Light", "UniGB-UCS2-H", false);
            //int i = 0;
            Iterator<String> it = fields.keySet().iterator();
            while (it.hasNext()) {
                //获取文本域名称
                String name = it.next().toString();
                //填充文本域
                //fields.get(name).setValue(str[i++]).setFont(font).setFontSize(12);

                if ((map.get(name) != null)) {
                    //单选框，不赋值和修改字体
                    if (!name.equals("zhuanghuang") && !name.equals("yunfei") && !name.equals("biaopei") && !name.equals("xuanpei")) {
                        //判断甲方乙方姓名，进行字体放大
                        //判断年月日，进行字体放大---大小10
                        //判断垫资，垫资服务费，港口服务费
                        //港口服务费
                        if (name.equals("username") || name.equals("brand")
                                || name.equals("date1") || name.equals("date2")
                                || name.equals("year") || name.equals("month") || name.equals("day")
                                || name.equals("jrallmoney") || name.equals("jrmoney") || name.equals("carprice2")
                                || name.equals("weikuan2") || name.equals("username3") || name.equals("address2")) {
                            fields.get(name).setValue(map.get(name) + "").setFont(font).setFontSize(10);
                        } else if (name.equals("qita0") || name.equals("qita1") || name.equals("qita2") || name.equals("qita3")
                                || name.equals("qita4") || name.equals("qita5") || name.equals("qita6") || name.equals("qita7")) {
                            fields.get(name).setValue(map.get(name) + "").setFont(font).setFontSize(10);
                        } else {
                            fields.get(name).setValue(map.get(name) + "").setFont(font).setFontSize(8);
                        }

                        if (name.equals("peizhishuoming")) {
                            fields.get(name).setValue((String) map.get(name)).setFontSize(7);
                            if (((String) map.get(name)).length() > 40) {
                                fields.get(name).setFontSize(4);
                            }
                        }
                    }
                }

                /*if(name.equals("zhuanghuang")||name.equals("yunfei")||name.equals("biaopei")||name.equals("xuanpei")){*/
                if (name.equals("jinrong1") || name.equals("jinrong2") || name.equals("biaopei") || name.equals("zhuanghuang")) {
                    if (map.get(name) != null && map.get(name).equals("yes")) {
                        fields.get(name).setCheckType(PdfFormField.TYPE_CHECK).setBorderWidth(0).setValue(map.get(name) + "").regenerateField();
                    }
                    //fields.get(name).regenerateField();
                  /*  fields.get(name);
                    if(map.get(name)==null||!map.get(name).equals("yes")){
                        //fields.get(name).regenerateField();
                        fields.get(name);
                    }else{
                    }*/
                }
                System.out.println(name);
            }
            form.flattenFields();//设置表单域不可编辑
            pdf.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return newPDFPath;
    }

    /**
     * @auther: Zy
     * @method t_Template
     * 功能描述: 使用pdf 模板生成 pdf 文件
     * @params: [realPath-生成路径, mobanPath-pdf文件距路径,fileName-文件名, map-数据]
     * @return: * @return : java.lang.String-新生成的pdf路径
     * @date: 2019-01-12 13:43:19
     */
    public static String t_Template(String realPath, String mobanPath, String fileName, HashMap map) {// 利用模板生成pdf
        // 模板路径
        String templatePath = mobanPath;
        // 生成的新文件路径
        String newPDFPath = realPath + fileName + ".pdf";
        try {
            PdfDocument pdf = new PdfDocument(new PdfReader(templatePath), new PdfWriter(newPDFPath));
            PdfAcroForm form = PdfAcroForm.getAcroForm(pdf, true);
            Map<String, PdfFormField> fields = form.getFormFields();
            //处理中文问题
            PdfFont font = PdfFontFactory.createFont("STSongStd-Light", "UniGB-UCS2-H", false);
            //int i = 0;
            Iterator<String> it = fields.keySet().iterator();
            while (it.hasNext()) {
                //获取文本域名称
                String name = it.next().toString();
                //填充文本域
                //fields.get(name).setValue(str[i++]).setFont(font).setFontSize(12);
                if ((map.get(name) != null)) {
                    //单选框，不赋值和修改字体
                    if (!name.equals("zhuanghuang") && !name.equals("yunfei") && !name.equals("biaopei") && !name.equals("xuanpei")) {
                        //判断甲方乙方姓名，进行字体放大
                        //判断年月日，进行字体放大---大小10
                        //判断垫资，垫资服务费，港口服务费
                        //港口服务费
                        if (name.equals("username") || name.equals("brand")
                                || name.equals("date1") || name.equals("date2")
                                || name.equals("year") || name.equals("month") || name.equals("day")
                                || name.equals("jrallmoney") || name.equals("jrmoney") || name.equals("carprice2")
                                || name.equals("weikuan2") || name.equals("username3") || name.equals("address2")) {
                            fields.get(name).setValue(map.get(name) + "").setFont(font).setFontSize(10);
                        } else if (name.equals("qita0") || name.equals("qita1") || name.equals("qita2") || name.equals("qita3")
                                || name.equals("qita4") || name.equals("qita5") || name.equals("qita6") || name.equals("qita7")) {
                            fields.get(name).setValue(map.get(name) + "").setFont(font).setFontSize(10);
                        } else {
                            fields.get(name).setValue(map.get(name) + "").setFont(font).setFontSize(8);
                        }

                        if (name.equals("peizhishuoming")) {
                            fields.get(name).setValue((String) map.get(name)).setFontSize(7);
                            if (((String) map.get(name)).length() > 40) {
                                fields.get(name).setFontSize(4);
                            }
                        }
                    }
                    if(name.equals("Text1")){
                        fields.get(name).setFont(font).setBackgroundColor(Color.WHITE);
                        fields.get(name).setFont(font).setColor(Color.BLACK);
                        fields.get(name).setFontSize(12);
                        System.out.println(fields.get(name).getValue()+"================");
                    }
                    if(name.equals("Text2")){
                        fields.get(name).setFont(font).setBackgroundColor(Color.WHITE);
                        fields.get(name).setFont(font).setColor(Color.BLACK);
                        fields.get(name).setFontSize(12);
                    }
                    if(name.equals("Text3")){
                        fields.get(name).setFont(font).setBackgroundColor(Color.WHITE);
                        fields.get(name).setFont(font).setColor(Color.BLACK);
                        fields.get(name).setFontSize(12);
                    }
                }
                System.out.println(name);
            }
            form.flattenFields();//设置表单域不可编辑
            pdf.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return newPDFPath;
    }

    public static void main(String[] args) {
        HashMap map = new HashMap();
        map.put("Text1", "我一巴掌能呼死你，信不？");
        map.put("Text2", "反正是信了，不知道你怎么想的");
        map.put("Text3", "反正是信了，不知道你怎么想的");
        String demo = t_Template("d:\\", "D:\\demo.pdf", "zhangDemo", map);
        PDF2IMAGE p = new PDF2IMAGE();
        p.pdf2Image(demo,"D:/",500);
        System.out.println(demo);
    }
}
