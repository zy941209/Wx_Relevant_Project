package cn.jsxz.common.utils.ExcelUtil;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@RequestMapping("/addController")
public class AddController {

    private static Log log = LogFactory.getLog(AddController.class);

    @RequestMapping(value = "batchimport", method = RequestMethod.POST)
    public String batchimport(@RequestParam(value="filename") MultipartFile file,
                              HttpServletRequest request,HttpServletResponse response) throws IOException {
        log.info("AddController ..batchimport() start");
        //判断文件是否为空
        if(file==null) return null;
        //获取文件名
        String name=file.getOriginalFilename();
        //进一步判断文件是否为空（即判断其大小是否为0或其名称是否为null）
        long size=file.getSize();
        if(name==null || ("").equals(name) && size==0) return null;

        //批量导入。参数：文件名，文件。
       // boolean b = customerService.batchImport(name,file);
        //创建处理EXCEL
        boolean b = false;
//        ReadExcel readExcel=new ReadExcel();
//        //解析excel，获取客户信息集合。
//        List<Customers> customerList = readExcel.getExcelInfo(name ,file);
//
//        if(customerList != null){
//            b = true;
//        }
        //迭代添加客户信息（注：实际上这里也可以直接将customerList集合作为参数，在Mybatis的相应映射文件中使用foreach标签进行批量添加。）
//        for(Customers customers:customerList){
//            //customerDoImpl.addCustomers(customers);
//            System.out.println(customers.toString());
//        }
        if(b){
            String Msg ="批量导入EXCEL成功！";
            request.getSession().setAttribute("msg",Msg);
        }else{
            String Msg ="批量导入EXCEL失败！";
            request.getSession().setAttribute("msg",Msg);
        }
        return "Customer/addCustomer3";
    }

}
