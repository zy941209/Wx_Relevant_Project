package cn.jsxz.common.utils;


import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import java.net.URL;

/**
 * @auther: Zy
 * @method EHCacheUtils
 * 功能描述: 单点登录
 * @date: 2019
 */
public class EHCacheUtils {
    private static final String path = "/ehcache.xml";
    private URL url;
    private CacheManager manager;

    private static EHCacheUtils ehCache;

    private EHCacheUtils(String path) {
        url = getClass().getResource(path);
        manager = CacheManager.create(url);
    }

    public static EHCacheUtils getInstance() {
        if (ehCache == null) {
            ehCache = new EHCacheUtils(path);
        }
        return ehCache;
    }

    public void put(String cacheName, String key, Object value) {
        Cache cache = manager.getCache(cacheName);
        Element element = new Element(key, value);
        cache.put(element);
    }

    public Object get(String cacheName, String key) {
        Cache cache = manager.getCache(cacheName);
        Element element = cache.get(key);
        return element == null ? null : element.getObjectValue();
    }

    public Cache get(String cacheName) {
        return manager.getCache(cacheName);
    }

    public void remove(String cacheName, String key) {
        Cache cache = manager.getCache(cacheName);
        cache.remove(key);
    }
}