package cn.jsxz.common.config;

import cn.jsxz.common.utils.ReadPropertiesUtil;
import com.baomidou.mybatisplus.core.exceptions.MybatisPlusException;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.ConstVal;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.FileOutConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.TemplateConfig;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

// 演示例子，执行 main 方法控制台输入模块表名回车自动生成对应项目目录中
public class MyGenerator {

    public static String b = "y";//是否继续-y是n否

    //运行
    public static void main(String[] args) {
        System.out.println("y".equalsIgnoreCase(b));
        codeGenerator();
    }

    /**
     * @auther: Zy
     * @method codeGenerator
     * 功能描述: 代码生成工具
     * @params: []
     * @return: * @return : void
     * @date: 2019-01-11 17:02:19
     */
    public static void codeGenerator() {
        // 代码生成器
        AutoGenerator mpg = new AutoGenerator();
        // 全局配置
        GlobalConfig gc = new GlobalConfig();
        gc.setBaseResultMap(true);
        String projectPath = System.getProperty("user.dir");
        System.out.println(projectPath);
        gc.setOutputDir(projectPath + "/src/main/java");//生成文件的输出目录
        gc.setAuthor("Zy");
        gc.setOpen(false);//是否打开输出目录
        gc.setEnableCache(false);//是否开启默认缓存t
        mpg.setGlobalConfig(gc);
        gc.setServiceName("%sService");//service 命名方式
        gc.setMapperName("%sDao");//mapper 命名方式
        gc.setFileOverride(true);//是否覆盖原来代码-默认false


        // 数据源配置
        ReadPropertiesUtil rpu = new ReadPropertiesUtil("config");
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUrl(rpu.getProValue("spring.datasource.url"));//数据库链接地址
        // dsc.setSchemaName("public");
        dsc.setDriverName(rpu.getProValue("spring.datasource.driverClassName"));//链接名
        dsc.setUsername(rpu.getProValue("spring.datasource.username"));//
        dsc.setPassword(rpu.getProValue("spring.datasource.password"));

        mpg.setDataSource(dsc);

        // 包配置
        PackageConfig pc = new PackageConfig();
//        pc.setModuleName(scanner("模块名"));
        pc.setParent("cn.jsxz");
        pc.setController("controller");//设置Controller包名
        pc.setMapper("dao");//设置mapper包名
        mpg.setPackageInfo(pc);

        // 自定义配置
        InjectionConfig cfg = new InjectionConfig() {
            @Override
            public void initMap() {
                // to do nothing
            }
        };

        // 如果模板引擎是 freemarker
        String templatePath = "/templates/mapper.xml.ftl";
        // 如果模板引擎是 velocity
//         String templatePath = "/templates/mapper.xml.vm";

        // 自定义输出配置
        List<FileOutConfig> focList = new ArrayList<>();
        // 自定义配置会被优先输出
        focList.add(new FileOutConfig(templatePath) {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输出文件名
                return projectPath + "/src/main/resources/mappers/" + tableInfo.getEntityName() + "Mapper" + StringPool.DOT_XML;
            }
        });
        cfg.setFileOutConfigList(focList);
        mpg.setCfg(cfg);

        // 配置模板
        TemplateConfig templateConfig = new TemplateConfig();

        // 配置自定义输出模板
//         templateConfig.setEntity("");
//         templateConfig.setService("");
//         templateConfig.setController("");
        templateConfig.setXml(ConstVal.TEMPLATE_XML);
        mpg.setTemplate(templateConfig);

        // 策略配置
        StrategyConfig strategy = new StrategyConfig();
        strategy.setNaming(NamingStrategy.underline_to_camel);//数据库表映射到实体的命名策略
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);//数据库表字段映射到实体的命名策略, 未指定按照 naming 执行
//        strategy.setSuperEntityClass("com.baomidou.mybatisplus.extension.activerecord.Model");
        strategy.entityTableFieldAnnotationEnable(true);//是否生成实体时，生成字段注解
        strategy.setRestControllerStyle(true);//生成 @RestController 控制器
        strategy.setInclude(scanner("输入表名"));
        strategy.setSuperEntityColumns("id");
        strategy.setControllerMappingHyphenStyle(true);//驼峰转连字符
        strategy.setTablePrefix(pc.getModuleName() + "_");
        mpg.setStrategy(strategy);
        mpg.setTemplateEngine(new FreemarkerTemplateEngine());
        mpg.execute();
        b = scanner("选择是否继续?");
        if ("y".equalsIgnoreCase(b)) {
            codeGenerator();
        } else {
            System.out.println("结束！");
        }
    }

    /**
     * 读取控制台内容
     */
    public static String scanner(String tip) {
        Scanner scanner = new Scanner(System.in);
        StringBuilder help = new StringBuilder();
        help.append("请" + tip + "：");
        System.out.println(help.toString());
        if (scanner.hasNext()) {
            String ipt = scanner.next();
            if (StringUtils.isNotEmpty(ipt)) {
                return ipt;
            }
        }
        throw new MybatisPlusException("请输入正确的" + tip + "！");
    }
}