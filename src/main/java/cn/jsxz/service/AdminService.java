package cn.jsxz.service;

import cn.jsxz.entity.Admin;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * 系统用户信息
 *
 * @author zy
 * @ClassName: AdminService
 * @Description: TODO
 * @date 2018年9月27日
 */
public interface AdminService extends IService<Admin> {


    /**
     * 查询用户基本信息
     *
     * @param limit  每页条数
     * @param page   当前页数
     * @param params
     * @return
     */
    @SuppressWarnings("rawtypes")
    IPage<Map> selectAdminList(Integer limit, Integer page, Map<String, Object> params);

}
