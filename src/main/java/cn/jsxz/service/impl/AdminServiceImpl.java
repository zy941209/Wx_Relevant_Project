package cn.jsxz.service.impl;

import cn.jsxz.dao.AdminDao;
import cn.jsxz.entity.Admin;
import cn.jsxz.service.AdminService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 后台管理impl
 *
 * @author zy
 * @ClassName: AdminServiceImpl
 * @Description: TODO
 */
@Service
public class AdminServiceImpl extends ServiceImpl<AdminDao, Admin> implements AdminService {

    @Autowired
    private AdminDao adminDao;


    @SuppressWarnings("rawtypes")
    @Override
    public IPage<Map> selectAdminList(Integer limit, Integer page, Map<String, Object> params) {
        IPage<Map> pageMap = new Page<>();
        pageMap.setSize(limit);
        pageMap.setCurrent(page);
        List<Map> clientInfoList = adminDao.selectAdminList(pageMap, params);
        pageMap.setRecords(clientInfoList);
        return pageMap;
    }
}
