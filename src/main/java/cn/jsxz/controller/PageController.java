package cn.jsxz.controller;

import cn.jsxz.common.utils.DateTimeUtil.DateUtil;
import cn.jsxz.entity.Admin;
import cn.jsxz.service.AdminService;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.crypto.SecureUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


/**
 * 页面跳转控制器
 *
 * @author Administrator
 * @ClassName: PageController
 * @Description: TODO
 */
@Controller
@RequestMapping("/page/")
public class PageController {

    @Autowired
    private AdminService adminService;

    /**
     * @auther: Zy
     * @method toLogin
     * 功能描述: 去登录页
     * @params:
     * @return: * @return : java.lang.String
     */
    @RequestMapping(value = "toLogin", method = RequestMethod.GET)
    public String toLogin() {
        return "login/login";
    }

    /**
     * @auther: Zy
     * @method toAdminIndex
     * 功能描述:去后台首页
     * @params:
     * @return: * @return : java.lang.String
     */
    @RequestMapping(value = "toAdminIndex", method = RequestMethod.GET)
    public String toAdminIndex() {
        return "index";
    }

    /**
     * @auther: Zy
     * @method showMain
     * 功能描述: 显示主页
     * @params:
     * @return: * @return : java.lang.String
     */
    @RequestMapping(value = "showMain", method = RequestMethod.GET)
    public String showMain() {
        return "main";
    }

    /**
     * @auther: Zy
     * @method toChangePassWord
     * 功能描述: 去改密码页面
     * @params: []
     * @return: * @return : java.lang.String
     */
    @RequestMapping(value = "toChangePassWord", method = RequestMethod.GET)
    public String toChangePassWord() {
        return "user/changePassWord";
    }

    /**
     * 后台退出登录
     *
     * @Title: tuichu @Description: TODO @param @param session @param @return
     * 参数 @return String 返回类型 @throws
     */
    @RequestMapping(value = "tuichu", method = RequestMethod.GET)
    public String tuichu(HttpSession session) {
        session.removeAttribute("user");
        return "login/login";
    }

    /**
     * 去系统用户管理修改页面
     */
    @RequestMapping(value = "toAdminEdit", method = RequestMethod.GET)
    public String toAdminEdit(HttpServletRequest request) {
        String adminId = request.getParameter("adminId");
        Admin admin = adminService.getById(adminId);
        request.setAttribute("admin", admin);
        return "admin/adminEdit";
    }

    /**
     * 去新增管理员页面
     */
    @RequestMapping(value = "toAdminAdd", method = RequestMethod.GET)
    public String toAdminAdd() {
        return "admin/adminAdd";
    }

    /**
     * 重置超管信息
     */
    @RequestMapping(value = "resetManager", method = RequestMethod.GET)
    public String resetAdmin() {
        // 判断登录名是否已经存在
        Admin a = adminService.getOne(new QueryWrapper<Admin>().eq("loginName", "admin").eq("identityFlag", 1));
        if (a != null) {
            a.setLoginPwd(SecureUtil.md5("1234"));
            adminService.updateById(a);
        } else {
            Admin admin = new Admin();
            admin.setAdminId(RandomUtil.simpleUUID());
            admin.setLoginName("admin");
            admin.setLoginPwd(SecureUtil.md5("1234"));
            admin.setCreateTime(DateUtil.getNowTime());
            admin.setUpdateTime(DateUtil.getNowTime());
            admin.setName("超级管理员");
            adminService.save(admin);
        }
        return "sbdw/login";
    }

    /**
     * 去系统用户管理页面
     */
    @RequestMapping(value = "toAdminList", method = RequestMethod.GET)
    public String toAdminList(HttpServletRequest request) {
        return "admin/adminList";
    }
}
