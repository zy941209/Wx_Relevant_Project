package cn.jsxz.dao;

import cn.jsxz.entity.Admin;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;

import java.util.List;
import java.util.Map;

/**
 * 后台管理员表 Mapper
 *
 * @author zy
 * @ClassName: AdminDao
 * @Description: TODO
 */

public interface AdminDao extends BaseMapper<Admin> {
    List<Map> selectAdminList(IPage<Map> page, Map param);
}
