package cn.jsxz.dto;

import io.swagger.annotations.ApiModelProperty;

/**
 * 请求返回的最外层对象
 *
 * @param <T>
 * @author Admin
 * @ClassName: Result
 * @Description: TODO
 */
public class Result<T> {
    /* 错误码 */
    @ApiModelProperty(value = "错误码", name = "错误码")
    private Integer code;
    /* 提示信息成功success */
    @ApiModelProperty(value = "错误码描述", name = "错误码描述")
    private String msg;
    /* 具体的内容 */
    @ApiModelProperty(value = "数据对象", name = "数据对象")
    private T data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
