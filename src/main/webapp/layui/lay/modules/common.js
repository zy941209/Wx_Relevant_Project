/**
 * Created by Leo.Tang on 2017/11/28.
 */ 
$(document).ready(function(){
    var hostUrl="http://192.168.1.127:8080";
    $.ajax({
        url: hostUrl + "/dianshang/api/topshop",
        data: {},
        type: "get",
        success: function (res) {
            var sjList="";
            for(var i=0;i<res.list.length;i++){
                sjList+="<li><span class='sjnum'>"+(i+1)+"</span><span class='sjname'>"+res.list[i].name+"</span>" +
                    "<span class='sjright'><span class='sjrt'><span>"+res.list[i].millsum+"万元</span><span>"+res.list[i].bfb+"</span></span>" +
                    "<span class='sjrb'><span class='bfb' style='width: "+res.list[i].bfb+"'></span></span></span></li>";
            }
            $(".sjList").html(sjList);
        },
        error: function (err) {
            console.log(err);
        }
    });
    $.ajax({
        url: hostUrl + "/dianshang/api/allshops",
        data: {},
        type: "get",
        success: function (res) {
            var addLi="";
            for(var i=0;i<res.list.length;i++){
                addLi+="<li><div class='s-left'>"+res.list[i].testRondomDate+"</div><div class='s-center'>"+res.list[i].shopname+"</div><div class='s-right'>"+res.list[i].name+"</div></li>";
            }
            $(".addLi").html(addLi);
            $(".xunhuan").myScroll({
                speed: 50, //数值越大，速度越慢
                rowHeight: 35 //li的高度
            });
        },
        error: function (err) {
            console.log(err);
        }
    });
    $.ajax({
        url: hostUrl + "/dianshang/api/dayAndWeekNumber",
        data: {},
        type: "get",
        success: function (res) {
            $(".head-cje").text(res.sumday);//日总额
            $(".rcj").text(res.shopnumday);//日成交
            $(".zcj").text(res.shopnumweek);//周成交
            $(".sjzs").text(res.companycount);//商家总数
            $(".cjze").text(res.sumday);//成交总额
            $(".cjdl").text(res.ordernum);//成交单量
            $(".fks").text(res.fksum);//访客数
            $(".lll").text(res.llsum);//浏览量
            $(".kdj").text(res.kdj);//客单价
            $(".sp1name").text(res.monthshoptwo[0].shopname);
            $(".sp1money").text(res.monthshoptwo[0].total);
            $(".sp2name").text(res.monthshoptwo[1].shopname);
            $(".sp2money").text(res.monthshoptwo[1].total);
            var myChart = echarts.init(document.getElementById('hxt'));
            option = {
                tooltip: {
                    trigger: 'item'
                },
                color: ['#4489ca', '#e74226'],
                legend: {
                    orient: 'vertical',
                    x: 'left'
                },
                series: [
                    {
                        name: '',
                        type: 'pie',
                        radius: ['50%', '70%'],
                        avoidLabelOverlap: false,
                        label: {
                            normal: {
                                show: false,
                                position: 'center'
                            },
                            emphasis: {
                                show: true,
                                textStyle: {
                                    fontSize: '20',
                                    fontWeight: 'bold'
                                }
                            }
                        },
                        labelLine: {
                            normal: {
                                show: false
                            }
                        },
                        data: [
                            {value: res.shopnumweek, name: '周成交数'},
                            {value: res.shopnumday, name: '日成交数'}
                        ]
                    }
                ]
            };
            myChart.setOption(option);
        },
        error: function (err) {
            console.log(err);
        }
    });
    $.ajax({
        url: hostUrl + "/dianshang/api/monthqushi",
        data: {},
        type: "get",
        success: function (res) {
            console.log(res);
            var myChart2 = echarts.init(document.getElementById('zxt'));
            option = {
                title: {
                    text: '当月交易额走势图',
                    x: 'center',
                    y: 'top',
                    textAlign: 'left',
                    textStyle: {
                        color: '#95cafe'
                    }
                },
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'cross',
                        label: {
                            backgroundColor: '#6a7985'
                        }
                    }
                },
                color: ['#4489ca'],
                grid: {
                    left: '3%',
                    right: '4%',
                    bottom: '3%',
                    containLabel: true
                },
                xAxis: [
                    {
                        type: 'category',
                        boundaryGap: false,
                        data: res.month,
                        axisLabel: {
                            show: true,
                            textStyle: {
                                color: '#95cafe'
                            }
                        }
                    }
                ],
                yAxis: [
                    {
                        type: 'value',
                        axisLabel: {
                            formatter: '{value}',
                            textStyle: {
                                color: '#95cafe'
                            }
                        }
                    }
                ],
                series: [
                    {
                        name: '成交额',
                        type: 'line',
                        areaStyle: {normal: {}},
                        data: res.total
                    }
                ]
            };
            myChart2.setOption(option);
        },
        error: function (err) {
            console.log(err);
        }
    });

});