<%--
  Created by IntelliJ IDEA.
  User: lucas
  Date: 2018/1/31
  Time: 下午5:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="common.jsp"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>平台收益记录--挪车后台</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="${ctx}/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="${ctx}/css/public.css" media="all" />
</head>
<body class="childrenBody">
<form class="layui-form">
    <blockquote class="layui-elem-quote quoteBox">
        <form class="layui-form">
            <div class="layui-inline">
                <div class="layui-inline">
                    <label class="layui-form-label">日期范围</label>
                    <div class="layui-input-inline">
                        <input type="text" class="layui-input" style="width: 190px;" id="dataselect" placeholder="-">
                    </div>
                </div>

                <a class="layui-btn search_btn" data-type="reload">搜索</a>
            </div>

        </form>
    </blockquote>
    <table id="incomeList" lay-filter="incomeList"></table>

    <!--用户身份-->
    <script type="text/html" id="userRole">
        {{#  if(d.userRole == "1"){ }}
        <span class="layui-btn layui-btn-xs  layui-btn-radius layui-btn-danger">学员</span>
        {{#  } else if(d.userRole == "2"){ }}
        <span class="layui-btn layui-btn-xs  layui-btn-radius layui-btn-warm">教练</span>
        {{#  } else if(d.userRole == "3") { }}
        <span class="layui-btn layui-btn-xs  layui-btn-radius layui-btn-normal">区域代理</span>
        {{#  } else if(d.userRole == "4") { }}
        <span class="layui-btn layui-btn-xs  layui-btn-radius">平台</span>
        {{#  }}}
    </script>

    <!--操作-->
</form>
<script type="text/javascript" src="${ctx}/layui/layui.js"></script>
<script>
    layui.use(['form','layer','table','laytpl','laydate'],function() {
        var form = layui.form,
            layer = parent.layer === undefined ? layui.layer : top.layer,
            $ = layui.jquery,
            laytpl = layui.laytpl,
            table = layui.table,
            laydate = layui.laydate;
        //日期范围
        laydate.render({
            elem: '#dataselect'
            ,range: true
        });


        //用户列表
        var tableIns = table.render({
            elem: '#incomeList',
            url : '/EstateLoan/income/getIncomeList',
            cellMinWidth : 95,
            page : true,
            height : "full-125",
            // limit : 10,
            // limits : [10,15,20,25],
            loading:true,
            id : "incomeListTable",
            cols : [[
                {type: "checkbox", fixed:"left", width:50},

                {field: 'income', title: '收益金额', minWidth:100, align:"center"},
                {field: 'gmtCreated', title: '收益时间', align:'center',minWidth:150},
            ]]
        });



        //搜索【此功能需要后台配合，所以暂时没有动态效果演示】
        $(".search_btn").on("click",function(){
            tableIns.reload({
                page: {
                    curr: 1 //重新从第 1 页开始
                },
                where: { //设定异步数据接口的额外参数，任意设
                    date:$("#dataselect").val()
                }
            })
        });
    });
</script>
</body>
</html>
