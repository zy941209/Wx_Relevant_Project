<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common.jsp" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>店铺账号管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="${ctx}/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="${ctx}/css/public.css" media="all"/>
</head>
<body class="childrenBody">
<form class="layui-form">
    <blockquote class="layui-elem-quote quoteBox">
        <div class="layui-inline">
      		<label class="layui-form-label">城市:</label>
            <div class="layui-inline">
	        	<select class="layui-select seller_province" lay-filter="seller_province"  id="seller_province" name="seller_province"  lay-verify="required" >
			        <option value=""></option>
			        <c:forEach items="${cityList}" var="obj">
			        	<option value="${obj.province}">${obj.province}</option>
			        </c:forEach>
		        </select>
            </div>
            <div class="layui-inline">
                <select class="layui-select seller_city" lay-filter="seller_city"  name="seller_city" id="seller_city">
                	<option value="">请选择地市</option>
		        </select>
            </div>
            </div>
            <div class="layui-inline">
	      		<label class="layui-form-label">店铺名称:</label>
	            <div class="layui-inline">
	                <select class="layui-select seller_name"  lay-filter="seller_name"  name="seller_name" id="seller_name">
	                	<option value="">请选择店铺</option>
			        </select>
	            </div>
            </div>
            <div class="layui-inline">
	      		<label class="layui-form-label">姓名:</label>
	            <div class="layui-inline">
	            <input type="text" class="layui-input searchVal" placeholder="姓名"/>
	            </div>
            </div>
            <div class="layui-inline">
                <a class="layui-btn search_btn" data-type="reload">搜索</a>
            </div>
            <!--<div class="layui-inline">
                <a class="layui-btn layui-btn-normal addAdmin_btn">添加</a>
            </div>-->
            <%--<div class="layui-inline">--%>
                <%--<a class="layui-btn layui-btn-danger layui-btn-normal delAll_btn">批量删除</a>--%>
            <%--</div>--%>
        </form>
    </blockquote>
    <table id="adminList" lay-filter="adminList"></table>

    <!--操作-->
   <!-- <script type="text/html" id="adminListBar">
        <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
        <a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="del">删除</a>
    </script>-->

    <script type="text/html" id="checkboxTpl">
  		<input type="checkbox" name="forbidden_flag" value="{{d.id}}" title="禁用" lay-filter="lockForbidden" {{ d.forbidden_flag == 1 ? 'checked' : '' }}>
	</script>
</form>
<script type="text/javascript" src="${ctx}/layui/layui.js"></script>
<script type="text/javascript" src="${ctx}/js/admin/sellerAdminList.js"></script>
</body>
</html>
