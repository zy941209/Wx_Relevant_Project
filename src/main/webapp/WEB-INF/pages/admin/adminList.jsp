<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="../common.jsp"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>用户管理</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="format-detection" content="telephone=no">
<link rel="stylesheet" href="${ctx}/layui/css/layui.css" media="all" />
<link rel="stylesheet" href="${ctx}/css/public.css" media="all" />
</head>
<body class="childrenBody">
	<form class="layui-form">
		<blockquote class="layui-elem-quote quoteBox">
			<form class="layui-form">
				<div class="layui-inline">
					<label class="layui-form-label">用户姓名:</label>
					<div class="layui-input-inline">
						<input type="text" class="layui-input adminName"  placeholder="姓名" />
					</div>
				</div>
				<div class="layui-inline">
					<a class="layui-btn search_btn" data-type="reload">搜索</a> <a
						class="layui-btn layui-btn-normal addAdmin_btn">添加</a>
				</div>
			</form>
		</blockquote>
		<table id="adminList" lay-filter="adminList"></table>

		<!--操作-->
		<script type="text/html" id="adminListBar">
        <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
        <a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="del">删除</a>
    </script>
    <script type="text/html" id="icon">
    	<div> <a  lay-event="lookImg"><img title="点击放大" style="cursor: pointer;"  width='30' height='30' higth id="myImg" src="{{ d.photo}}"/></a>  </div>
	</script>
	</form>
	<script type="text/javascript" src="${ctx}/layui/layui.js"></script>
	<script type="text/javascript" src="${ctx}/js/admin/adminList.js"></script>
</body>
</html>
