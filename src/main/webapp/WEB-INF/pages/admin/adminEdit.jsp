<%--
  Created by IntelliJ IDEA.
  User: lucas
  Date: 2018/2/2
  Time: 上午10:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="../common.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>修改</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="format-detection" content="telephone=no">
<link rel="stylesheet" href="${ctx}/layui/css/layui.css" media="all" />
<link rel="stylesheet" href="${ctx}/css/public.css" media="all" />
<style>
._citys {
	width: 450px;
	display: inline-block;
	border: 2px solid #eee;
	padding: 5px;
	position: relative;
}

._citys span {
	color: #56b4f8;
	height: 15px;
	width: 15px;
	line-height: 15px;
	text-align: center;
	border-radius: 3px;
	position: absolute;
	right: 10px;
	top: 10px;
	border: 1px solid #56b4f8;
	cursor: pointer;
}

._citys0 {
	width: 100%;
	height: 34px;
	display: inline-block;
	border-bottom: 2px solid #56b4f8;
	padding: 0;
	margin: 0;
}

._citys0 li {
	display: inline-block;
	line-height: 34px;
	font-size: 15px;
	color: #888;
	width: 80px;
	text-align: center;
	cursor: pointer;
}

.citySel {
	background-color: #56b4f8;
	color: #fff !important;
}

._citys1 {
	width: 100%;
	display: inline-block;
	padding: 10px 0;
}

._citys1 a {
	width: 83px;
	height: 35px;
	display: inline-block;
	background-color: #f5f5f5;
	color: #666;
	margin-left: 6px;
	margin-top: 3px;
	line-height: 35px;
	text-align: center;
	cursor: pointer;
	font-size: 13px;
	overflow: hidden;
}

._citys1 a:hover {
	color: #fff;
	background-color: #56b4f8;
}

.AreaS {
	background-color: #56b4f8 !important;
	color: #fff !important;
}
.imageFlot {
	margin-left: 8%;
}
</style>
</head>
<body class="childrenBody">
	<form class="layui-form" style="width:80%;">
		<div class="layui-upload">
			<label class="layui-form-label">头像:</label>
			<div class="imageFlot">
				<div class="layui-upload-list" id="imgPreviewFront">
					<img style="width: 120px; height: 120px; "
						src="${admin.photo}" alt="" class="layui-upload-img">
				</div>
				<input type="hidden" name="identityIamgeFront"
					id="identityIamgeFront" value="${admin.photo}" />
				<button type="button" class="layui-btn layui-btn-primary"
					id="fileBtnFront">
					<i class="layui-icon">&#xe67c;</i>选择文件(jpg|png|gif)
				</button>
				<button type="button" class="layui-btn layui-btn-warm"
					id="uploadBtnFront">开始上传</button>
			</div>
		</div>
		<br/>
		<div class="layui-form-item layui-row layui-col-xs12">
			<label class="layui-form-label">登录名</label>
			<div class="layui-input-block">
				<input type="text" class="layui-input loginName" id="loginName"
					value="${admin.loginName}" name="loginName" lay-verify="required"
					 placeholder="登录名">
			</div>
		</div>
		<div class="layui-form-item layui-row layui-col-xs12">
			<label class="layui-form-label">登录密码</label>
			<div class="layui-input-block">
				<input type="text" class="layui-input password" id="password"
					value="" name="password" placeholder="为空时不修改">
			</div>
		</div>
		<div class="layui-form-item layui-row layui-col-xs12">
			<label class="layui-form-label">姓名</label>
			<div class="layui-input-block">
				<input type="text" class="layui-input adminName" id="adminName"
					value="${admin.name}" name="adminName" lay-verify="adminName"
					placeholder="姓名">
			</div>
		</div>
		<div class="layui-form-item layui-row layui-col-xs12">
			<label class="layui-form-label">手机号</label>
			<div class="layui-input-block">
				<input type="text" class="layui-input adminPhone" id="adminPhone"
					value="${admin.phone}" name="adminPhone" lay-verify="required"
					placeholder="手机号">
			</div>
		</div>

		<div class="layui-form-item layui-row layui-col-xs12">
			<div class="layui-input-block">
				<input type="hidden" id="adminId" name="adminId" class="adminId"
					value="${admin.adminId}">
				<button class="layui-btn layui-btn-sm" lay-submit=""
					lay-filter="edit">立即提交</button>
				<%--<button type="reset" class="layui-btn layui-btn-sm layui-btn-primary">取消</button>--%>
			</div>
		</div>
	</form>
	<script src="${ctx}/layui/jquery.min.js"></script>
	<script type="text/javascript" src="${ctx}/layui/layui.js"></script>
	<%--<script type="text/javascript" src="${ctx}/js/admin/adminEdit.js"></script>--%>
	<script>
        layui.use([ 'form', 'layer' ], function() {
            var form = layui.form
            layer = parent.layer === undefined ? layui.layer : top.layer,
                $ = layui.jquery,

                form.on("submit(edit)", function(data) {
                    //弹出loading
                    var index = top.layer.msg('数据提交中，请稍候', {
                        icon : 16,
                        time : false,
                        shade : 0.8
                    });
                    // 实际使用时的提交信息
                    $.ajax({
                        url : '/direct/admin/adminUpdate',
                        method : 'post',
                        dataType : 'json',
                        data : {
                            adminId : data.field.adminId,
                            userPhoto : data.field.identityIamgeFront,
                            loginName : data.field.loginName,
                            loginPwd : data.field.password,
                            adminName : data.field.adminName,
                            adminPhone : data.field.adminPhone
                        },
                        success : function(res) {
                            if (res.code == 200) {
                                top.layer.close(index);
                                top.layer.msg("操作成功！", {
                                    icon : 6
                                });
                                parent.location.reload();
                            } else {
                                top.layer.msg(res.msg, {
                                    icon : 5
                                });
                            }
                        },
                        error : function() {
                            top.layer.msg("服务器繁忙,请稍后再试！");
                        }
                    });
                    return false;
                })
            layui.use('upload', function() {
                var upload = layui.upload;
                upload.render({
                    elem : '#fileBtnFront',
                    url : '../qiniu/uploadOneImg',
                    accept : 'file',
                    auto : false,
                    exts : 'jpg|png', //只允许上传压缩文件
                    bindAction : '#uploadBtnFront',
                    before : function(obj) {
                        $("#identityIamgeFront").val("");
                        //预读本地文件示例，不支持ie8
                        obj.preview(function(index, file, result) { //在当前ID为“imgPreview”的区域显示图片
                            $('#imgPreviewFront').empty();
                            $('#imgPreviewFront').append('<img name = "s_pmt_dw"  style="width: 120px; height: 120px;" src="' + result + '" alt="' + file.name + '" class="layui-upload-img">')
                        });
                    },
                    done : function(res) {
                        console.log(res.data.src);
                        $("#identityIamgeFront").val(res.data.src);
                        //	             DrawImage(res.data.src,200,200);
                    }
                });
            });

        })
	</script>
</body>
</html>
