layui.use([ 'form', 'layer', 'jquery' ], function() {
	var form = layui.form,
		layer = parent.layer === undefined ? layui.layer : top.layer
	$ = layui.jquery;


	//登录按钮
	form.on('submit(login)', function(data) {
		$(this).text("登录中...").attr("disabled", "disabled").addClass("layui-disabled");

		$.ajax({
			url : '../admin/login',
			type : 'POST', //GET
			async : true, //或false,是否异步
			data : data.field,
			timeout : 5000, //超时时间
			dataType : 'json', //返回的数据格式：json/xml/html/script/jsonp/text
			success : function(result) {
				console.log(result);
				if (result.code == 200) {
					if ((result.data).adminLeve == 1) {
						window.location.href = "../page/toAdminIndex";
					}else if((result.data).adminLeve == 2){
						window.location.href = "/demo1_1/page/toChindAdminIndex";
					}
				} else if (result.code == 100) {
					layer.msg(result.msg, {
						icon : 5,
						time : 800
					});
				} else if (result.code == 400) {
					layer.msg(result.msg);
				}
			},
			error : function() {
				layer.msg('系统繁忙请稍后再试', {
					icon : 5,
					time : 800
				});
			},
		// complete:function(){
		//     console.log('结束')
		// }
		})
		$(this).text("登录").removeAttr("disabled", "disabled").removeClass("layui-disabled");
		return false;
	})

	//表单输入效果
	$(".loginBody .input-item").click(function(e) {
		e.stopPropagation();
		$(this).addClass("layui-input-focus").find(".layui-input").focus();
	})
	$(".loginBody .layui-form-item .layui-input").focus(function() {
		$(this).parent().addClass("layui-input-focus");
	})
	$(".loginBody .layui-form-item .layui-input").blur(function() {
		$(this).parent().removeClass("layui-input-focus");
		if ($(this).val() != '') {
			$(this).parent().addClass("layui-input-active");
		} else {
			$(this).parent().removeClass("layui-input-active");
		}
	})
})