layui.use(['form','layer'],function(){
    var form = layui.form
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,

    form.on("submit(edit)",function(data){
        //弹出loading
        var index = top.layer.msg('数据提交中，请稍候',{icon: 16,time:false,shade:0.8});
        var password=$("#password").val();
        var confirmpassword=$("#confirmpassword").val();
        if(password!=confirmpassword){
        	top.layer.msg("输入密码不一致");
        	return false;
        }
        // 实际使用时的提交信息
        $.ajax({
            url:'/direct/admin/changePwd',
            method:'post',
            dataType:'json',
            data:{
                id:data.field.id,
                loginPwd:data.field.password
            },
            success: function(res){
                if (res.code == 200){
                    top.layer.close(index);
                    top.layer.msg("操作成功！",{icon: 6});
                    parent.location.reload();
                }else {
                    top.layer.msg(res.msg,{icon: 5});
                }
            },
            error: function () {
                top.layer.msg("服务器繁忙,请稍后再试！");
            }
        });
        return false;
    })
  //添加验证规则  
    form.verify({     
//        oldPwd : function(value, item){  
//                if (value.length == 0) {  
//                    return '请输入旧密码';  
//                }  
//                var msg = '';  
//                $.ajax({  
//                    url: "/Welcome/getAjaxPassWord/",  
//                    type: "post",  
//                    async: false,  
//                    data: {  
//                        uname:$('#userAdmin').text(),  
//                        upass:value  
//                    },  
//                    dataType: "html",  
//                    success: function (data) {  
//                       msg = data;  
//                    }  
//                });  
//                if(msg!='1'){return "旧密码输入错误，请重新输入！";}  
//            },  
        newPwd : function(value, item){  
            if(value.length < 6){  
                return "密码长度不能小于6位";  
            }  
        },  
        confirmPwd : function(value, item){  
            if(!new RegExp($("#password").val()).test(value)){  
                return "两次输入密码不一致，请重新输入！";  
            }  
        }  
    }); 
})