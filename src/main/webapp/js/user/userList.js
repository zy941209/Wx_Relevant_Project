layui.use([ 'form', 'layer', 'table', 'laytpl', 'laydate' ], function() {
	var form = layui.form,
		layer = parent.layer === undefined ? layui.layer : top.layer,
		$ = layui.jquery,
		laytpl = layui.laytpl,
		table = layui.table,
		laydate = layui.laydate;
	//地址url列表
	var tableIns = table.render({
		elem : '#adminList',
		url : '/direct/user/getUserList',
		cellMinWidth : 95,
		page : true,
		method : 'post',
		height : "full-175",
		loading : true,
		id : "adminListTable",
		cols : [ [
			{type : "checkbox",fixed : "left",width : 50},
			{field : 'userHead',title : '头像',width : 80,align : "center",templet : "#icon"},
			{field : 'nickName',title : '昵称',width : 160,align : "center"},
			{field : 'phone',title : '手机号',width : 160,align : "center"},
			{field : 'userStatus',title : '状态',align : 'center',width : 70,
				templet : function(d) {
					var js = d.userStatus;
					if (js == 0) {
						return "正常";
					} else if (js == 1) {
						return "封禁";
					}
				}
			},
			{field : 'work',title : '静态余额',width : 130,align : "center"},
			{field : 'dynamicBalance',title : '动态收益额度',width : 130,align : "center"},
            {field : 'createTime',title : '注册时间',width : 150,align : "center"},
			{title : '操作',width : 475,templet : '#adminListBar',fixed : "right",align : "center"}
		] ]

	});

	//搜索【此功能需要后台配合，所以暂时没有动态效果演示】
	$(".search_btn").on("click", function() {
		tableIns.reload({
			page : {
				curr : 1 //重新从第 1 页开始
			},
			where : { //请求参数
				phone : $("#phone").val(),
				nickName : $("#nickName").val()
			}
		})
	});
	
	function detail(detail) {
		var index = layui.layer.open({
			title : "操作系统用户",
			type : 2,
			content : "/EstateLoan/user/queryUserInfo?userId=" + detail.userId,
			success : function(layero, index) {
				setTimeout(function() {
					layui.layer.tips('点击此处返回列表', '.layui-layer-setwin .layui-layer-close', {
						tips : 3
					});
				}, 500)
			}
		})
		layui.layer.full(index);
		//改变窗口大小时，重置弹窗的宽高，防止超出可视区域（如F12调出debug的操作）
		$(window).on("resize", function() {
			layui.layer.full(index);
		})
	}
	//禁止借贷
	function editAdmin(obj) {
		var loanDisable = obj.loanDisable;
		var msg ="是否禁止该用户借贷操作?";
		if(loanDisable==1){
			loanDisable = 0;
			msg = "是否启用该用户借贷？";
		}else{
			loanDisable = 1;
		}
		layer.confirm(msg,
			{
				icon : 3,
				title : '提示',
				skin : "skin-green"
			}, function(index) {
				$.ajax({
					url : '/EstateLoan/user/userUpdate',
					type : 'POST',
					async : false,
					dataType : "json",
					data : {
						userId : obj.userId,
						loanDisable : loanDisable
					},
					success : function(data) {
						if (data.code == 200) {
							top.layer.msg("操作成功！", {
								icon : 6
							});
							tableIns.reload();
							return true;
						} else {
							top.layer.msg(data.msg, {
								icon : 5
							});
							return false;
						}
						top.layer.close(loginLoading);
					},
					error : function() {
						top.layer.msg("网络异常！", {
							icon : 5
						});
						return false;
					}
				});
			})
	}

	//列表操作
	table.on('tool(adminList)', function(obj) {
		var layEvent = obj.event,
			//当前行数据
			data = obj.data;
		if (layEvent === 'edit') { //编辑
			editAdmin(data);
		}
		if (layEvent === 'detail') { //详情
			detail(data);
		}
        if (layEvent === 'del') { //编辑
        	delAdmin(data);
        }
	});

	//日期时间选择器
	laydate.render({
		elem : '#startTime',
		type : 'datetime'
	});
	//日期时间选择器
	laydate.render({
		elem : '#endTime',
		type : 'datetime'
	});
	$(document).on('click', '#myImg', function(data) {
		var image = new Image();
		image.src = data.currentTarget.src;
		var h = 300;
		var w = 300;
		layer.open({
			closeBtn : 0,
			shift : 3,
			shadeClose : true, // 点击遮罩关闭层
			area : [ w + "px", h + "px" ],
			type : 1,
			title : false,
			offset : 30 + 'px',
			content : "<span><img style='height:" + (h - 20) + "px;width:" + (w - 20) + "px;margin-left:10px;margin-top:10px;' src=" + data.currentTarget.src + " /></span>" //注意，如果str是object，那么需要字符拼接。
		}, ($(this)), {
			open : [ 2, '#5CBA59' ],
			time : 0
		});
	});
});