layui.use(['form', 'layer', 'table', 'laytpl', 'laydate'], function () {
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        laytpl = layui.laytpl,
        table = layui.table,
        laydate = layui.laydate;
    //地址url列表
    var tableIns = table.render({
        elem: '#adminList',
        url: '/demo1_1/admin/getAdminList',
        cellMinWidth: 95,
        page: true,
        method:'post',
        height: "full-175",
        loading: true,
        id: "adminListTable",
        cols: [[
            {type: "checkbox", fixed: "left", width: 50},
            {field : 'photo',title : '头像',width : 80,align : "center",templet : "#icon"},
            {field: 'loginName', title: '登录名', minWidth: 100, align: "center"},
            {field: 'name', title: '姓名', minWidth: 100, align: "center"},
            {field: 'phone', title: '电话', align: 'center', minWidth: 150},
            {field : 'adminLeve',title : '身份状态',align : 'center',width : 100,
                templet : function(d) {
                    var js = d.adminLeve;
                    if (js == 1) {
                        return "超级管理员";
                    } else if (js == 2) {
                        return "子管理员";
                    }
                }
            },
            {field: 'createTime', title: '创建时间', align: 'center', minWidth: 150},
            {title: '操作', minWidth: 175, templet: '#adminListBar', fixed: "right", align: "center"}
        ]]
    });

    //搜索【此功能需要后台配合，所以暂时没有动态效果演示】
    $(".search_btn").on("click", function () {
        tableIns.reload({
            page: {
                curr: 1 //重新从第 1 页开始
            },
            where: { //请求参数
            	adminName: $(".adminName").val()
            }
        })
    });

    //编辑
    function editAdmin(edit) {
    	console.log(edit);
        var index = layui.layer.open({
            title: "操作系统用户",
            type: 2,
            content: "/direct/page/toAdminEdit?adminId="+edit.adminId,
            success: function (layero, index) {
                setTimeout(function () {
                    layui.layer.tips('点击此处返回管理员列表', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                }, 500)
            }
        })
        layui.layer.full(index);
        //改变窗口大小时，重置弹窗的宽高，防止超出可视区域（如F12调出debug的操作）
        $(window).on("resize", function () {
            layui.layer.full(index);
        })
    }

    //新增
    function  addAdmin() {
        var index = layui.layer.open({
            title: "新增",
            type: 2,
            content: "/direct/page/toAdminAdd"
        })
        layui.layer.full(index);
        //改变窗口大小时，重置弹窗的宽高，防止超出可视区域（如F12调出debug的操作）
        $(window).on("resize", function () {
            layui.layer.full(index);
        })
    }
    //删除
    function delAdmin(obj){
    	 layer.confirm("是否删除" + obj.loginName + "用户信息！",
                 {
                     icon: 3,
                     title: '提示',
                     skin: "skin-green"
                 }, function (index) {
                     $.ajax({
                         url: '/direct/admin/toDelAdmin ',
                         type: 'POST',
                         async: false,
                         dataType: "json",
                         data: {adminId:obj.adminId},
                         success: function (data) {
                             if (data.code == 200) {
                                 top.layer.msg("操作成功！", {icon: 6});
                                 tableIns.reload();
                                 return true;
                             } else {
                                 top.layer.msg(data.msg, {icon: 5});
                                 return false;
                             }
                             top.layer.close(loginLoading);
                         }, error: function () {
                             top.layer.msg("网络异常！", {icon: 5});
                             return false;
                         }
                     });

                 })
    }
    $(".addAdmin_btn").click(function () {
        addAdmin();
    });



    //列表操作
    table.on('tool(adminList)', function (obj) {
        var layEvent = obj.event,
            //当前行数据
            data = obj.data;

        if (layEvent === 'edit') { //编辑
            editAdmin(data);
        }
        if (layEvent === 'del') { //删除
        	delAdmin(data);
        }
        
    });


       $(document).on('click', '#myImg', function(data) {
   		var image = new Image();
   		image.src = data.currentTarget.src;
   		var h = 300;
   		var w = 300;
   		layer.open({
   			closeBtn : 0,
   			shift : 3,
   			shadeClose : true, // 点击遮罩关闭层
   			area : [ w + "px", h + "px" ],
   			type : 1,
   			title : false,
   			offset : 30 + 'px',
   			content : "<span><img style='height:" + (h - 20) + "px;width:" + (w - 20) + "px;margin-left:10px;margin-top:10px;' src=" + data.currentTarget.src + " /></span>" //注意，如果str是object，那么需要字符拼接。
   		}, ($(this)), {
   			open : [ 2, '#5CBA59' ],
   			time : 0
   		});
   	});
});