layui.use([ 'form', 'layer' ], function() {
	var form = layui.form
	layer = parent.layer === undefined ? layui.layer : top.layer,
	$ = layui.jquery,

	form.on("submit(edit)", function(data) {
		//弹出loading
		var index = top.layer.msg('数据提交中，请稍候', {
			icon : 16,
			time : false,
			shade : 0.8
		});
		// 实际使用时的提交信息
		$.ajax({
			url : '/idrect/admin/adminUpdate',
			method : 'post',
			dataType : 'json',
			data : {
				adminId : data.field.adminId,
				photo : data.field.identityIamgeFront,
				loginName : data.field.loginName,
				loginPwd : data.field.password,
				adminName : data.field.adminName,
				adminPhone : data.field.adminPhone
			},
			success : function(res) {
				if (res.code == 200) {
					top.layer.close(index);
					top.layer.msg("操作成功！", {
						icon : 6
					});
					parent.location.reload();
				} else {
					top.layer.msg(res.msg, {
						icon : 5
					});
				}
			},
			error : function() {
				top.layer.msg("服务器繁忙,请稍后再试！");
			}
		});
		return false;
	})
	layui.use('upload', function() {
		var upload = layui.upload;
		upload.render({
			elem : '#fileBtnFront',
			url : '../admin/uploadOneImg',
			accept : 'file',
			auto : false,
			exts : 'jpg|png', //只允许上传压缩文件
			bindAction : '#uploadBtnFront',
			before : function(obj) {
				$("#identityIamgeFront").val("");
				//预读本地文件示例，不支持ie8
				obj.preview(function(index, file, result) { //在当前ID为“imgPreview”的区域显示图片
					$('#imgPreviewFront').empty();
					$('#imgPreviewFront').append('<img name = "s_pmt_dw"  style="width: 120px; height: 120px;" src="' + result + '" alt="' + file.name + '" class="layui-upload-img">')
				});
			},
			done : function(res) {
				console.log(res.data.src);
				$("#identityIamgeFront").val(res.data.src);
			//	             DrawImage(res.data.src,200,200);
			}
		});
	});

})